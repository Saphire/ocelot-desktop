name := "ocelot-desktop"
version := "1.6.1"
scalaVersion := "2.13.10"

lazy val root = project.in(file("."))
  .dependsOn(brain % "compile->compile")
  .aggregate(brain)
  .enablePlugins(BuildInfoPlugin)
  .settings(
    buildInfoKeys := Seq[BuildInfoKey](
      BuildInfoKey.action("commit") {
        scala.sys.process.Process("git rev-parse HEAD").!!.trim
      }, version
    )
  )

lazy val brain = ProjectRef(file("lib/ocelot-brain"), "ocelot-brain")

libraryDependencies += "org.apache.logging.log4j" % "log4j-core" % "2.20.0"
libraryDependencies += "org.apache.logging.log4j" % "log4j-api" % "2.20.0"
libraryDependencies += "org.apache.logging.log4j" % "log4j-slf4j-impl" % "2.20.0"
libraryDependencies += "li.flor" % "native-j-file-chooser" % "1.6.4"

val lwjglVersion = "2.9.3"

libraryDependencies += "org.lwjgl.lwjgl" % "lwjgl" % lwjglVersion
libraryDependencies += "org.lwjgl.lwjgl" % "lwjgl-platform" % lwjglVersion classifier "natives-linux"
libraryDependencies += "org.lwjgl.lwjgl" % "lwjgl-platform" % lwjglVersion classifier "natives-windows"
libraryDependencies += "org.lwjgl.lwjgl" % "lwjgl-platform" % lwjglVersion classifier "natives-osx"

libraryDependencies += "com.github.stephengold" % "j-ogg-all" % "1.0.3"
libraryDependencies += "com.github.wendykierp" % "JTransforms" % "3.1"
libraryDependencies += "com.github.sarxos" % "webcam-capture" % "0.3.12"

// For OpenFM
libraryDependencies += "com.googlecode.soundlibs" % "mp3spi" % "1.9.5.4"
libraryDependencies += "com.googlecode.soundlibs" % "vorbisspi" % "1.0.3.3"

assembly / assemblyMergeStrategy := {
  case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
  case _ => MergeStrategy.first
}

assembly / assemblyJarName := s"ocelot-desktop.jar"
