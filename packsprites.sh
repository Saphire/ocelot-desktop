#!/usr/bin/bash

if ! command -v spritepack &> /dev/null
then
    echo "spritepack could not be found in the PATH variable!"
    exit
fi

spritepack -s 512x512 -o ./src/main/resources/ocelot/desktop/images/spritesheet -n spritesheet ./sprites/
