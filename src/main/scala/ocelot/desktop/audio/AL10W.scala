package ocelot.desktop.audio

import ocelot.desktop.util.Logging
import org.lwjgl.openal.AL10

import java.nio.{ByteBuffer, IntBuffer}

object AL10W extends Logging {
  private def run[T](func: String)(f: => T): T = {
    val res = f
    val err = AL10.alGetError()
    if (err != AL10.AL_NO_ERROR) {
      val errName = classOf[AL10].getDeclaredFields.find(field => {
        try {
          field.getInt() == err
        } catch {
          case _: Exception => false
        }
      }).map(_.getName).getOrElse(err.toHexString)
      logger.error(s"OpenAL error: ${func}: ${errName}")
    }
    res
  }

  def alIsExtensionPresent(name: String): Boolean = run("alIsExtensionPresent") {
    AL10.alIsExtensionPresent(name)
  }

  def alGenBuffers(): Int = run("alGenBuffers") {
    AL10.alGenBuffers()
  }

  def alBufferData(buffer: Int, format: Int, data: ByteBuffer, freq: Int): Unit = run("alBufferData") {
    AL10.alBufferData(buffer, format, data, freq)
  }

  def alGetBufferi(buffer: Int, pname: Int): Int = run("alGetBufferi") {
    AL10.alGetBufferi(buffer, pname)
  }

  def alDeleteBuffers(buffer: Int): Unit = run("alDeleteBuffers") {
    AL10.alDeleteBuffers(buffer)
  }

  def alGenSources(): Int = run("alGenSources") {
    AL10.alGenSources()
  }

  def alSourceQueueBuffers(source: Int, buffer: Int): Unit = run("alSourceQueueBuffers") {
    AL10.alSourceQueueBuffers(source, buffer)
  }

  def alSourceUnqueueBuffers(source: Int, buffers: IntBuffer): Unit = run("alSourceUnqueueBuffers") {
    AL10.alSourceUnqueueBuffers(source, buffers)
  }

  def alGetSourcei(source: Int, pname: Int): Int = run("alGetSourcei") {
    AL10.alGetSourcei(source, pname)
  }

  def alSourcei(source: Int, pname: Int, value: Int): Unit = run("alSourcei") {
    AL10.alSourcei(source, pname, value)
  }

  def alSourcef(source: Int, pname: Int, value: Float): Unit = run("alSourcef") {
    AL10.alSourcef(source, pname, value)
  }

  def alSource3f(source: Int, pname: Int, v1: Float, v2: Float, v3: Float): Unit = run("alSource3f") {
    AL10.alSource3f(source, pname, v1, v2, v3)
  }

  def alSourcePlay(source: Int): Unit = run("alSourcePlay") {
    AL10.alSourcePlay(source)
  }

  def alSourcePause(source: Int): Unit = run("alSourcePause") {
    AL10.alSourcePause(source)
  }

  def alSourceStop(source: Int): Unit = run("alSourceStop") {
    AL10.alSourceStop(source)
  }

  def alDeleteSources(source: Int): Unit = run("alDeleteSources") {
    AL10.alDeleteSources(source)
  }
}
