package ocelot.desktop.audio

import ocelot.desktop.Settings
import ocelot.desktop.util.Logging
import org.lwjgl.LWJGLException
import org.lwjgl.openal.{AL, AL10, ALC10}

import java.nio.{ByteBuffer, ByteOrder}
import scala.collection.mutable

object Audio extends Logging {
  val sampleRate: Int = 44100

  private val sources = new mutable.HashMap[SoundSource, Int]
  private var _disabled = true

  /**
    * Should be called _before_ initializing any sound-related resources
    */
  def init(): Unit = {
    try {
      AL.create()
      logger.info(s"OpenAL device: ${ALC10.alcGetString(AL.getDevice, ALC10.ALC_DEVICE_SPECIFIER)}")
      _disabled = false
    } catch {
      case e: LWJGLException =>
        logger.error("Unable to initialize OpenAL. Disabling sound")
        e.printStackTrace()
    }
  }

  def isDisabled: Boolean = _disabled

  def numSources: Int = synchronized {
    sources.size
  }

  def newStream(soundCategory: SoundCategory.Value, pitch: Float = 1f,
                volume: Float = 1f): (SoundStream, SoundSource) =
  {
    var source: SoundSource = null

    val stream = new SoundStream {
      override def enqueue(samples: SoundSamples): Unit = Audio.synchronized {
        val sourceId = if (sources.contains(source)) {
          sources(source)
        } else {
          val sourceId = AL10W.alGenSources()

          AL10W.alSourcef(sourceId, AL10.AL_PITCH, source.pitch)
          AL10W.alSource3f(sourceId, AL10.AL_POSITION, 0f, 0f, 0f)
          AL10W.alSourcef(sourceId, AL10.AL_GAIN, source.volume * SoundCategory.getSettingsValue(source.soundCategory) * Settings.get.volumeMaster)
          sources.put(source, sourceId)

          sourceId
        }

        cleanupSourceBuffers(sourceId)

        val bufferId = samples.genBuffer()
        AL10W.alSourceQueueBuffers(sourceId, bufferId)
        if (AL10W.alGetSourcei(sourceId, AL10.AL_SOURCE_STATE) != AL10.AL_PLAYING)
          AL10W.alSourcePlay(sourceId)
      }
    }

    source = SoundSource.fromStream(stream, soundCategory, looping = false, pitch, volume)
    (stream, source)
  }

  def getSourceStatus(source: SoundSource): SoundSource.Status.Value = synchronized {
    if (!sources.contains(source))
      return SoundSource.Status.Stopped

    val sourceId = sources(source)
    AL10W.alGetSourcei(sourceId, AL10.AL_SOURCE_STATE) match {
      case AL10.AL_PLAYING => SoundSource.Status.Playing
      case AL10.AL_PAUSED => SoundSource.Status.Paused
      case _ => SoundSource.Status.Stopped
    }
  }

  def playSource(source: SoundSource): Unit = synchronized {
    if (getSourceStatus(source) == SoundSource.Status.Playing)
      return

    if (sources.contains(source)) {
      AL10W.alSourcePlay(sources(source))
      return
    }

    val sourceId = AL10W.alGenSources()
    source.kind match {
      case SoundSource.KindSoundBuffer(buffer) =>
        AL10W.alSourcei(sourceId, AL10.AL_BUFFER, buffer.bufferId)
      case SoundSource.KindSoundSamples(samples) =>
        val bufferId = samples.genBuffer()
        if (bufferId != -1) AL10W.alSourceQueueBuffers(sourceId, bufferId)
      case SoundSource.KindStream(_) =>
    }

    AL10W.alSourcef(sourceId, AL10.AL_PITCH, source.pitch)
    AL10W.alSource3f(sourceId, AL10.AL_POSITION, 0f, 0f, 0f)
    AL10W.alSourcei(sourceId, AL10.AL_LOOPING, if (source.looping) AL10.AL_TRUE else AL10.AL_FALSE)
    AL10W.alSourcef(sourceId, AL10.AL_GAIN, source.volume * SoundCategory.getSettingsValue(source.soundCategory) * Settings.get.volumeMaster)
    AL10W.alSourcePlay(sourceId)

    sources.put(source, sourceId)
  }

  def pauseSource(source: SoundSource): Unit = synchronized {
    if (getSourceStatus(source) == SoundSource.Status.Paused)
      return

    if (sources.contains(source)) {
      AL10W.alSourcePause(sources(source))
    }
  }

  def stopSource(source: SoundSource): Unit = synchronized {
    if (getSourceStatus(source) == SoundSource.Status.Stopped)
      return

    if (sources.contains(source)) {
      AL10W.alSourceStop(sources(source))
    }
  }

  def update(): Unit = synchronized {
    if (isDisabled) return

    sources.filterInPlace { case (source, sourceId) =>
      cleanupSourceBuffers(sourceId)

      AL10W.alSourcef(sourceId, AL10.AL_GAIN, source.volume * SoundCategory.getSettingsValue(source.soundCategory) * Settings.get.volumeMaster)
      AL10W.alGetSourcei(sourceId, AL10.AL_SOURCE_STATE) match {
        case AL10.AL_STOPPED =>
          deleteSource(sourceId)
          false
        case _ => true
      }
    }
  }

  def destroy(): Unit = synchronized {
    if (isDisabled) return

    for ((_, sourceId) <- sources) {
      deleteSource(sourceId)
    }

    sources.clear()
    AL.destroy()
    _disabled = true
  }

  private def deleteSource(sourceId: Int): Unit = {
    AL10W.alSourceStop(sourceId)
    cleanupSourceBuffers(sourceId)
    AL10W.alDeleteSources(sourceId)
  }

  private def cleanupSourceBuffers(sourceId: Int): Unit = {
    val count = AL10W.alGetSourcei(sourceId, AL10.AL_BUFFERS_PROCESSED)
    if (count <= 0) return

    val buff = ByteBuffer.allocateDirect(count * 4)
    buff.order(ByteOrder.nativeOrder())
    val buf = buff.asIntBuffer()

    AL10W.alSourceUnqueueBuffers(sourceId, buf)

    for (i <- 0 until count) {
      AL10W.alDeleteBuffers(buf.get(i))
    }
  }
}
