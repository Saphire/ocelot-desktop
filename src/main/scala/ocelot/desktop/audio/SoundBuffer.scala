package ocelot.desktop.audio

import ocelot.desktop.util.{FileUtils, Logging, Resource, ResourceManager}
import org.lwjgl.openal.AL10

class SoundBuffer(val file: String) extends Resource with Logging {
  private var _bufferId: Int = -1

  ResourceManager.registerResource(this)

  override def initResource(): Unit = {
    if (Audio.isDisabled)
      return

    logger.debug(s"Loading sound buffer from '$file'...")
    _bufferId = AL10W.alGenBuffers()

    if (AL10W.alIsExtensionPresent("AL_EXT_vorbis")) {
      initWithExt()
    } else {
      initFallback()
    }
  }

  private def initWithExt(): Unit = {
    val fileBuffer = FileUtils.load(file)
    if (fileBuffer == null) {
      logger.error(s"Could not load '$file'!")
      return
    }

    AL10W.alBufferData(bufferId, AL10.AL_FORMAT_VORBIS_EXT, fileBuffer, -1)
  }

  private def initFallback(): Unit = {
    val ogg = OggDecoder.decode(getClass.getResourceAsStream(file))
    _bufferId = ogg.genBuffer()
  }

  def numSamples: Int = {
    if (bufferId == -1) {
      return 0
    }

    val sizeBytes = AL10W.alGetBufferi(bufferId, AL10.AL_SIZE)
    val channels = AL10W.alGetBufferi(bufferId, AL10.AL_CHANNELS)
    val bits = AL10W.alGetBufferi(bufferId, AL10.AL_BITS)

    sizeBytes * 8 / channels / bits
  }

  val sampleRate: Int = {
    if (bufferId == -1) {
      44100
    } else {
      AL10W.alGetBufferi(bufferId, AL10.AL_FREQUENCY)
    }
  }

  def bufferId: Int = _bufferId

  def freeResource(): Unit = {
    if (bufferId != -1) {
      AL10W.alDeleteBuffers(bufferId)
      logger.debug(s"Destroyed sound buffer (ID: $bufferId) loaded from $file")
    }
  }
}
