package ocelot.desktop.audio

object SoundBuffers {
  val MachineComputerRunning = new SoundBuffer("/ocelot/desktop/sounds/machine/computer_running.ogg")
  val MachineFloppyAccess: Array[SoundBuffer] = Array(
    new SoundBuffer("/ocelot/desktop/sounds/machine/floppy_access1.ogg"),
    new SoundBuffer("/ocelot/desktop/sounds/machine/floppy_access2.ogg"),
    new SoundBuffer("/ocelot/desktop/sounds/machine/floppy_access3.ogg"),
    new SoundBuffer("/ocelot/desktop/sounds/machine/floppy_access4.ogg"),
    new SoundBuffer("/ocelot/desktop/sounds/machine/floppy_access5.ogg"),
    new SoundBuffer("/ocelot/desktop/sounds/machine/floppy_access6.ogg"),
  )
  val MachineFloppyEject = new SoundBuffer("/ocelot/desktop/sounds/machine/floppy_eject.ogg")
  val MachineFloppyInsert = new SoundBuffer("/ocelot/desktop/sounds/machine/floppy_insert.ogg")
  val MachineHDDAccess: Array[SoundBuffer] = Array(
    new SoundBuffer("/ocelot/desktop/sounds/machine/hdd_access1.ogg"),
    new SoundBuffer("/ocelot/desktop/sounds/machine/hdd_access2.ogg"),
    new SoundBuffer("/ocelot/desktop/sounds/machine/hdd_access3.ogg"),
    new SoundBuffer("/ocelot/desktop/sounds/machine/hdd_access4.ogg"),
    new SoundBuffer("/ocelot/desktop/sounds/machine/hdd_access5.ogg"),
    new SoundBuffer("/ocelot/desktop/sounds/machine/hdd_access6.ogg"),
  )
  val InterfaceClick = new SoundBuffer("/ocelot/desktop/sounds/interface/click.ogg")
  val InterfaceTick = new SoundBuffer("/ocelot/desktop/sounds/interface/tick.ogg")
  val MinecraftClick = new SoundBuffer("/ocelot/desktop/sounds/minecraft/click.ogg")
  val MinecraftExplosion = new SoundBuffer("/ocelot/desktop/sounds/minecraft/explosion.ogg")

  val NoteBlock: Map[String, SoundBuffer] = List(
    "banjo", "basedrum", "bass", "bell", "bit", "chime", "cow_bell", "didgeridoo", "flute", "guitar",
    "harp", "hat", "iron_xylophone", "pling", "snare", "xylophone"
  ).map(name => {
    (name, new SoundBuffer(s"/ocelot/desktop/sounds/minecraft/note_block/$name.ogg"))
  }).toMap
}
