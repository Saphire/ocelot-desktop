package ocelot.desktop.audio

import ocelot.desktop.Settings

//noinspection ScalaWeakerAccess
object SoundCategory extends Enumeration {
  val Environment, Beep, Interface = Value

  def getSettingsValue(soundCategory: SoundCategory.Value): Float = soundCategory match {
    case Environment => Settings.get.volumeEnvironment
    case Beep => Settings.get.volumeBeep
    case Interface => Settings.get.volumeInterface
  }
}
