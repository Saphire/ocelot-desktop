package ocelot.desktop.audio

//noinspection TypeAnnotation
object SoundSources {
  val InterfaceClick = SoundSource.fromBuffer(SoundBuffers.InterfaceClick, SoundCategory.Interface)
  val InterfaceClickLow = SoundSource.fromBuffer(SoundBuffers.InterfaceClick, SoundCategory.Interface, pitch = 0.8f)
  val InterfaceTick = SoundSource.fromBuffer(SoundBuffers.InterfaceTick, SoundCategory.Interface)

  val MinecraftClick = SoundSource.fromBuffer(SoundBuffers.MinecraftClick, SoundCategory.Interface)
  val MinecraftExplosion = SoundSource.fromBuffer(SoundBuffers.MinecraftExplosion, SoundCategory.Environment)
}
