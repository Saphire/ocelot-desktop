package ocelot.desktop.graphics

import ocelot.desktop.color.{Color, RGBAColorNorm}
import ocelot.desktop.geometry.{Rect2D, Size2D, Transform2D, Vector2D}
import ocelot.desktop.graphics.mesh.{Mesh, MeshInstance}
import ocelot.desktop.graphics.render.InstanceRenderer
import ocelot.desktop.ui.UiHandler
import ocelot.desktop.util.{Font, Logging, Resource, Spritesheet}
import org.lwjgl.opengl.{ARBFramebufferObject, GL11, GL30}

import java.nio.ByteBuffer
import scala.collection.mutable
import scala.util.control.Breaks._

//noinspection ScalaWeakerAccess,ScalaUnusedSymbol
class Graphics extends Logging with Resource {
  private var time = 0f

  private var projection = Transform2D.viewport(800, 600)
  private var width = 800
  private var height = 600

  private val shaderProgram = new ShaderProgram("general")
  private val renderer = new InstanceRenderer(Mesh.quad, shaderProgram)

  private val normalFont = new Font("unscii-16", 16)
  private val smallFont = new Font("unscii-8", 8)
  private var _font: Font = normalFont
  private var oldFont: Font = _font

  private val stack = mutable.Stack[GraphicsState](GraphicsState())
  private var spriteRect = Spritesheet.sprites("Empty")
  private val emptySpriteTrans = Transform2D.translate(spriteRect.x, spriteRect.y) >> Transform2D.scale(spriteRect.w, spriteRect.h)

  private val offscreenTexture = new Texture(width, height, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE)

  private val offscreenFramebuffer = ARBFramebufferObject.glGenFramebuffers()
  GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, offscreenFramebuffer)
  GL30.glFramebufferTexture2D(GL30.GL_FRAMEBUFFER, GL30.GL_COLOR_ATTACHMENT0, GL11.GL_TEXTURE_2D,
    offscreenTexture.texture, 0)
  GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, 0)

  shaderProgram.set("uTexture", 0)
  shaderProgram.set("uTextTexture", 1)

  def resize(width: Int, height: Int): Unit = {
    offscreenTexture.bind()
    GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB, width, height, 0, GL11.GL_RGB,
      GL11.GL_UNSIGNED_BYTE, null.asInstanceOf[ByteBuffer])
    GL11.glViewport(0, 0, width, height)
  }

  override def freeResource(): Unit = {
    logger.debug(s"Destroyed FBO (ID: $offscreenFramebuffer)")
    GL30.glDeleteFramebuffers(offscreenFramebuffer)
  }

  def font: Font = _font

  def setNormalFont(): Unit = {
    if (_font == normalFont) return
    flush()
    _font = normalFont
  }

  def setSmallFont(): Unit = {
    if (_font == smallFont) return
    flush()
    _font = smallFont
  }

  GL11.glEnable(GL11.GL_BLEND)
  GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA)

  def save(): Unit = {
    stack.push(stack.head.copy())
  }

  def restore(): Unit = {
    if (stack.length == 1)
      throw new RuntimeException("attempt to pop root graphics state")

    val old = stack.pop()
    if (old.scissor != stack.head.scissor) {
      stack.head.scissor match {
        case Some(v) =>
          setScissor(v._1, v._2, v._3, v._4)

        case None =>
          setScissor()
      }
    }
  }

  def beginGroupAlpha(): Unit = {
    flush()
    GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, offscreenFramebuffer)
    GL30.glBindFramebuffer(GL30.GL_READ_FRAMEBUFFER, 0)
    GL30.glBindFramebuffer(GL30.GL_DRAW_FRAMEBUFFER, offscreenFramebuffer)
    GL30.glBlitFramebuffer(0, 0, width, height, 0, 0, width, height, GL11.GL_COLOR_BUFFER_BIT, GL11.GL_NEAREST)
  }

  def endGroupAlpha(alpha: Float): Unit = {
    flush()
    GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, 0)
    offscreenTexture.bind()
    foreground = RGBAColorNorm(1, 1, 1, alpha)
    spriteRect = Rect2D(0, 1f, 1f, -1f)
    _rect(0, 0, width, height, fixUV = false)
    renderer.flush()
  }

  def setViewport(width: Int, height: Int): Unit = {
    projection = Transform2D.viewport(width, height)
    this.width = width
    this.height = height

    shaderProgram.set("uProj", projection)
  }

  def clear(): Unit = {
    flush()
    GL11.glClearColor(0.95f, 0.95f, 0.95f, 1.0f)
    GL11.glClear(GL11.GL_COLOR_BUFFER_BIT)
  }

  def setScissor(x: Int, y: Int, width: Int, height: Int): Unit = {
    flush()
    stack.head.scissor = Some((x, y, width, height))
    GL11.glScissor(x, this.height - height - y, width, height)
    GL11.glEnable(GL11.GL_SCISSOR_TEST)
  }

  def setScissor(x: Float, y: Float, width: Float, height: Float): Unit = {
    setScissor(x.round, y.round, width.round, height.round)
  }

  def setScissor(): Unit = {
    flush()
    stack.head.scissor = None
    GL11.glDisable(GL11.GL_SCISSOR_TEST)
  }

  def clear(x: Int, y: Int, width: Int, height: Int): Unit = {
    setScissor(x, y, width, height)
    clear()
    setScissor()
  }

  def foreground: RGBAColorNorm = stack.head.foreground

  def foreground_=(col: Color): Unit = {
    stack.head.foreground = col.toRGBANorm
  }

  def background: RGBAColorNorm = stack.head.background

  def background_=(col: Color): Unit = {
    stack.head.background = col.toRGBANorm
  }

  def fontSizeMultiplier: Float = stack.head.fontSizeMultiplier

  def fontSizeMultiplier_=(value: Float): Unit = {
    stack.head.fontSizeMultiplier = value
  }

  def alphaMultiplier: Float = stack.head.alphaMultiplier

  def alphaMultiplier_=(value: Float): Unit = {
    stack.head.alphaMultiplier = value
  }

  def sprite: String = stack.head.sprite

  def sprite_=(value: String): Unit = {
    stack.head.sprite = value
    spriteRect = Spritesheet.sprites(value)
  }

  def transform(t: Transform2D): Unit = {
    stack.head.transform = stack.head.transform >> t
  }

  def scale(v: Float): Unit = {
    transform(Transform2D.scale(v))
  }

  def scale(x: Float, y: Float): Unit = {
    transform(Transform2D.scale(x, y))
  }

  def translate(x: Float, y: Float): Unit = {
    transform(Transform2D.translate(x, y))
  }

  def rotate(angle: Float): Unit = {
    transform(Transform2D.rotate(angle))
  }

  def text(x: Float, y: Float, text: String, shrink: Int = 0): Unit = {
    var ox = x

    for (c <- text) {
      char(ox, y, c)
      ox += _font.charWidth(c) - shrink
    }
  }

  def char(_x: Float, _y: Float, c: Int, scaleX: Float = 1f, scaleY: Float = 1f): Unit = {
    val fontSize = fontSizeMultiplier * _font.fontSize
    val x = _x.round
    val y = _y.round

    val rect = _font.map.getOrElse(c, _font.map('?'))

    val width = _font.charWidth(c) * scaleX
    val height = fontSize * scaleY

    val uvTransform = Transform2D.translate(
      rect.x,
      rect.y
    ) >> Transform2D.scale(
      rect.w - 0.25f / _font.AtlasWidth,
      rect.h - 0.25f / _font.AtlasHeight
    )

    val transform =
      stack.head.transform >>
        Transform2D.translate(x, y) >>
        Transform2D.scale(width, height)

    val foreground = stack.head.foreground.toRGBANorm.mapA(_ * alphaMultiplier)
    val background = stack.head.background.toRGBANorm.mapA(_ * alphaMultiplier)

    if (background.a > 0 || foreground.a > 0)
      renderer.schedule(MeshInstance(background, foreground, transform, emptySpriteTrans, uvTransform))
  }

  // I hate scala. Overloaded methods with default arguments are not allowed
  def sprite(icon: IconDef, bounds: Rect2D): Unit = {
    sprite(icon.icon, bounds.x, bounds.y, bounds.w, bounds.h, icon.color, icon.animation)
  }

  def sprite(name: String, bounds: Rect2D): Unit = {
    sprite(name, bounds.origin, bounds.size, RGBAColorNorm(1f, 1f, 1f))
  }

  def sprite(name: String, x: Float, y: Float, color: Color): Unit = {
    sprite(name, Vector2D(x, y), Spritesheet.spriteSize(name), color)
  }

  def sprite(name: String, pos: Vector2D, color: Color): Unit = {
    sprite(name, pos, Spritesheet.spriteSize(name), color)
  }

  def sprite(name: String, pos: Vector2D, size: Size2D, color: Color): Unit = {
    sprite(name, pos.x, pos.y, size.width, size.height, color)
  }

  def sprite(name: String, pos: Vector2D, size: Size2D): Unit = {
    sprite(name, pos.x, pos.y, size.width, size.height)
  }

  def sprite(name: String, x: Float, y: Float, width: Float, height: Float,
             color: Color = RGBAColorNorm(1f, 1f, 1f),
             animation: Option[IconDef.Animation] = None): Unit =
  {
    sprite = name
    foreground = color
    _rect(x, y, width, height, fixUV = true, animation)
  }

  def rect(r: Rect2D, color: Color): Unit = {
    rect(r.x, r.y, r.w, r.h, color)
  }

  def rect(x: Float, y: Float, width: Float, height: Float, color: Color = RGBAColorNorm(1f, 1f, 1f)): Unit = {
    sprite("Empty", x, y, width, height, color)
  }

  private def checkFont(): Unit = {
    if (_font != oldFont) {
      val newFont = _font
      _font = oldFont
      flush()
      _font = newFont
      oldFont = _font
    }
  }

  private def _rect(x: Float, y: Float, width: Float, height: Float,
                    fixUV: Boolean = true,
                    animation: Option[Array[(Int, Float)]] = None): Unit = {
    val spriteRect = animation match {
      case None => this.spriteRect
      case Some(frames) =>
        val duration = frames.map(_._2).sum
        var timeOffset = 0f
        var curFrame = 0

        breakable {
          for ((idx, dur) <- frames) {
            timeOffset += dur
            curFrame = idx
            if (timeOffset >= time % duration) break
          }
        }

        this.spriteRect.copy(y = this.spriteRect.y + curFrame * this.spriteRect.w, h = this.spriteRect.w)
    }

    val uvTransform = Transform2D.translate(spriteRect.x, spriteRect.y) >>
      (if (fixUV)
        Transform2D.scale(spriteRect.w - 0.25f / 512, spriteRect.h - 0.25f / 512)
      else
        Transform2D.scale(spriteRect.w, spriteRect.h))

    val transform =
      stack.head.transform >>
        Transform2D.translate(x, y) >>
        Transform2D.scale(width, height)

    val color = stack.head.foreground.toRGBANorm.mapA(_ * alphaMultiplier)

    if (color.a > 0)
      renderer.schedule(MeshInstance(color, Color.Transparent, transform, uvTransform, Transform2D.scale(0)))
  }

  def line(x1: Float, y1: Float, x2: Float, y2: Float, thickness: Float, color: Color): Unit = {
    save()

    val dy = x2 - x1
    val dx = y2 - y1
    val length = math.sqrt(dx * dx + dy * dy).toFloat
    val inclination = math.atan2(dy, dx).toFloat
    translate(x1, y1)
    rotate(-inclination + (math.Pi * 0.5).toFloat)
    rect(0, -thickness * 0.5f, length, thickness, color)

    restore()
  }

  def line(start: Vector2D, end: Vector2D, thickness: Float, color: Color): Unit = {
    line(start.x, start.y, end.x, end.y, thickness, color)
  }

  def flush(): Unit = {
    Spritesheet.texture.bind()
    _font.texture.bind(1)
    renderer.flush()
  }

  def update(): Unit = {
    time += UiHandler.dt * 20f
  }
}
