package ocelot.desktop.graphics

import ocelot.desktop.color.Color

case class IconDef(
  icon: String,
  sizeMultiplier: Float = 2f,
  animation: Option[IconDef.Animation] = None,
  color: Color = Color.White
)

object IconDef {
  type Animation = Array[(Int, Float)]
}
