package ocelot.desktop.graphics.render

import ocelot.desktop.graphics.ShaderProgram
import ocelot.desktop.graphics.buffer.{IndexBuffer, VertexBuffer}
import ocelot.desktop.graphics.mesh.{Mesh, MeshInstance, MeshVertex, VertexArray}
import ocelot.desktop.util.Logging
import org.lwjgl.BufferUtils
import org.lwjgl.opengl._

import java.nio.ByteBuffer
import scala.collection.mutable.ArrayBuffer

class InstanceRenderer(mesh: Mesh, shader: ShaderProgram) extends Logging {
  private val InitialCapacity: Int = 4096

  private val vertexBuffer = new VertexBuffer[MeshVertex](mesh.vertices)
  private val indexBuffer: Option[IndexBuffer] = mesh.indices.map(indices => new IndexBuffer(indices))
  private val instanceBuffer = new VertexBuffer[MeshInstance](MeshInstance, InitialCapacity)

  private val vertexArray = new VertexArray(shader)

  vertexArray.addVertexBuffer(vertexBuffer)
  indexBuffer.foreach(ib => vertexArray.addIndexBuffer(ib))
  vertexArray.addVertexBuffer(instanceBuffer, instanced = true)

  private val instances = new ArrayBuffer[MeshInstance]

  def schedule(instance: MeshInstance): Unit = {
    instances += instance
  }

  def flush(): Unit = {
    write()
    draw()
  }

  private lazy val (glDrawArraysInstancedARBptr, glDrawElementsInstancedARBptr) = {
    val fun = classOf[GLContext].getDeclaredMethod("getFunctionAddress", classOf[String])
    fun.setAccessible(true)
    (fun.invoke(null, "glDrawArraysInstancedARB"), fun.invoke(null, "glDrawElementsInstancedARB"))
  }

  private def draw(): Unit = {
    if (instances.isEmpty) return

    shader.bind()
    vertexArray.bind()

    indexBuffer match {
      case Some(ib) =>
        val method = classOf[ARBDrawInstanced].getDeclaredMethod("nglDrawElementsInstancedARBBO",
          Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, java.lang.Long.TYPE)
        method.setAccessible(true)
        method.invoke(null, GL11.GL_TRIANGLES, ib.capacity, GL11.GL_UNSIGNED_INT, 0, instances.length, glDrawElementsInstancedARBptr)
      case None =>
        val method = classOf[ARBDrawInstanced].getDeclaredMethod("nglDrawArraysInstancedARB",
          Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, java.lang.Long.TYPE)
        method.setAccessible(true)
        method.invoke(null, GL11.GL_TRIANGLES, 0, vertexBuffer.capacity, instances.length, glDrawArraysInstancedARBptr)
    }

    instances.clear()
  }

  private def write(): Unit = {
    if (instances.length > instanceBuffer.capacity) {
      instanceBuffer.resize(instances.length * 2)
      instanceBuffer.write(0, packInstances)
    } else {
      instanceBuffer.write(0, packInstances)
    }
  }

  private def packInstances: ByteBuffer = {
    val data = BufferUtils.createByteBuffer(instances.length * MeshInstance.stride)

    for (element <- instances) {
      element.put(data)
    }

    data.flip
    data
  }
}
