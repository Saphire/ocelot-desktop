package ocelot.desktop.node

import ocelot.desktop.OcelotDesktop
import ocelot.desktop.color.{Color, RGBAColor}
import ocelot.desktop.geometry.{Rect2D, Size2D, Vector2D}
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.UiHandler
import ocelot.desktop.ui.event.handlers.{ClickHandler, DragHandler, HoverHandler}
import ocelot.desktop.ui.event.{ClickEvent, DragEvent, HoverEvent, MouseEvent}
import ocelot.desktop.ui.widget.contextmenu.{ContextMenu, ContextMenuEntry}
import ocelot.desktop.ui.widget.window.Window
import ocelot.desktop.ui.widget.{InputDialog, Widget, WorkspaceView}
import ocelot.desktop.util.DrawUtils
import ocelot.desktop.util.animation.ColorAnimation
import org.lwjgl.input.Keyboard
import totoro.ocelot.brain.entity.traits.{Entity, Environment, SidedEnvironment}
import totoro.ocelot.brain.nbt.NBTTagCompound
import totoro.ocelot.brain.network
import totoro.ocelot.brain.util.Direction

import scala.collection.mutable.ArrayBuffer

abstract class Node(val entity: Entity with Environment) extends Widget with DragHandler with ClickHandler with HoverHandler {
  if (!OcelotDesktop.workspace.getEntitiesIter.contains(entity))
    OcelotDesktop.workspace.add(entity)

  var workspaceView: WorkspaceView = _

  protected val MovingHighlight: RGBAColor = RGBAColor(240, 250, 240)
  protected val HoveredHighlight: RGBAColor = RGBAColor(160, 160, 160)
  protected val NoHighlight: RGBAColor = RGBAColor(160, 160, 160, 0)
  protected val highlight = new ColorAnimation(RGBAColor(0, 0, 0, 0))
  protected val canOpen = false
  protected def exposeAddress = true

  protected var isMoving = false
  private var grabPoint: Vector2D = Vector2D(0, 0)

  protected val _connections: ArrayBuffer[(NodePort, Node, NodePort)] = ArrayBuffer[(NodePort, Node, NodePort)]()

  size = minimumSize

  def load(nbt: NBTTagCompound): Unit = {
    position = new Vector2D(nbt.getCompoundTag("pos"))
    window.foreach(window => {
      val tag = nbt.getCompoundTag("window")
      window.load(tag)
    })

    val lbl = nbt.getString("label")
    label = if (lbl == "") None else Some(lbl)
  }

  def save(nbt: NBTTagCompound): Unit = {
    val address = entity match {
      case env: SidedEnvironment =>
        env.sidedNode(Direction.South).address
      case _ => if (entity.node == null) null else entity.node.address
    }

    if (address != null && address.nonEmpty)
      nbt.setString("address", address)

    val posTag = new NBTTagCompound
    position.save(posTag)
    nbt.setTag("pos", posTag)

    window.foreach(window => {
      val tag = new NBTTagCompound
      window.save(tag)
      nbt.setTag("window", tag)
    })

    nbt.setString("label", label.getOrElse(""))
  }

  override def receiveMouseEvents = true

  eventHandlers += {
    case event: ClickEvent =>
      onClick(event)

    case DragEvent(DragEvent.State.Start, MouseEvent.Button.Left, pos) =>
      startMoving(pos)

    case DragEvent(DragEvent.State.Drag, MouseEvent.Button.Left, pos) =>
      move(pos)

    case DragEvent(DragEvent.State.Stop, MouseEvent.Button.Left, _) =>
      stopMoving()

    case DragEvent(DragEvent.State.Start, MouseEvent.Button.Right, pos) =>
      val port = portsBounds.flatMap(p => p._2.map(a => (p._1, a))).minBy(p => (p._2.center - pos).lengthSquared)._1
      workspaceView.newConnection = Some((this, port, pos))

    case DragEvent(DragEvent.State.Drag, MouseEvent.Button.Right, pos) =>
      workspaceView.newConnection = Some((this, workspaceView.newConnection.get._2, pos))

    case DragEvent(DragEvent.State.Stop, MouseEvent.Button.Right, _) =>
      workspaceView.buildNewConnection()

    case HoverEvent(HoverEvent.State.Enter) =>
      if (!isMoving)
        highlight.goto(HoveredHighlight)

    case HoverEvent(HoverEvent.State.Leave) =>
      if (!isMoving)
        highlight.goto(NoHighlight)
  }

  def setupContextMenu(menu: ContextMenu): Unit = {
    if (exposeAddress) { // TODO: lift the restriction
      menu.addEntry(new ContextMenuEntry(
        "Set label",
        () => new InputDialog(
          "Set label",
          text => label = if (text.isEmpty) None else Some(text),
          label.getOrElse("")
        ).show()
      ))
    }

    if (exposeAddress && entity.node != null && entity.node.address != null) {
      menu.addEntry(new ContextMenuEntry("Copy address", () => {
        UiHandler.clipboard = entity.node.address
      }))
    }

    menu.addEntry(new ContextMenuEntry("Disconnect", () => {
      disconnectFromAll()
    }))

    menu.addEntry(new ContextMenuEntry("Delete", destroy))
  }

  override def update(): Unit = {
    super.update()
    if (isHovered || isMoving) {
      if (canOpen)
        root.get.statusBar.addMouseEntry("icons/LMB", "Open")
      root.get.statusBar.addMouseEntry("icons/RMB", "Menu")
      root.get.statusBar.addMouseEntry("icons/DragLMB", "Move node")
      root.get.statusBar.addMouseEntry("icons/DragRMB", "Connect/Disconnect")
    }
  }

  def icon: String = "icons/NA"

  def iconColor: Color = RGBAColor(255, 255, 255)

  def ports: Array[NodePort] = Array(NodePort())

  def getNodeByPort(port: NodePort): network.Node = entity.node

  def shouldReceiveEventsFor(address: String): Boolean = address == entity.node.address

  def connections: Iterator[(NodePort, Node, NodePort)] = _connections.iterator

  def connect(portA: NodePort, node: Node, portB: NodePort): Unit = {
    this._connections.append((portA, node, portB))
    node._connections.append((portB, this, portA))
    this.onConnectionAdded(portA, node, portB)
    node.onConnectionAdded(portB, this, portA)
  }

  def disconnect(portA: NodePort, node: Node, portB: NodePort): Unit = {
    this._connections -= ((portA, node, portB))
    node._connections -= ((portB, this, portA))
    this.onConnectionRemoved(portA, node, portB)
    node.onConnectionRemoved(portB, this, portA)
  }

  def destroy() = {
    dispose()
    workspaceView.nodes = workspaceView.nodes.filter(_ != this)
  }

  def disconnectFromAll(): Unit = {
    for ((a, node, b) <- connections.toArray) {
      disconnect(a, node, b)
    }
  }

  def isConnected(portA: NodePort, node: Node, portB: NodePort): Boolean = {
    _connections.contains((portA, node, portB))
  }

  def onConnectionAdded(portA: NodePort, node: Node, portB: NodePort): Unit = {
    getNodeByPort(portA).connect(node.getNodeByPort(portB))
  }

  def onConnectionRemoved(portA: NodePort, node: Node, portB: NodePort): Unit = {
    getNodeByPort(portA).disconnect(node.getNodeByPort(portB))
  }

  def onClick(event: ClickEvent): Unit = {
    event match {
      case ClickEvent(MouseEvent.Button.Left, _) =>
        window.foreach(window => workspaceView.windowPool.openWindow(window))
      case ClickEvent(MouseEvent.Button.Right, _) =>
        val menu = new ContextMenu
        setupContextMenu(menu)
        root.get.contextMenus.open(menu)
      case _ =>
    }
  }

  def portsBounds: Iterator[(NodePort, Array[Rect2D])] = {
    val length = -4
    val thickness = 4
    val stride = thickness + 4
    val hsize = Size2D(length, thickness)
    val vsize = Size2D(thickness, length)

    val centers = bounds.edgeCenters
    val ports = this.ports
    val numPorts = ports.length

    ports.sorted.iterator.zipWithIndex.map { case (port, portIdx) =>
      val top = Rect2D(centers(0) + Vector2D(-thickness / 2, -length), vsize)
      val right = Rect2D(centers(1) + Vector2D(0, -thickness / 2), hsize)
      val bottom = Rect2D(centers(2) + Vector2D(-thickness / 2, 0), vsize)
      val left = Rect2D(centers(3) + Vector2D(-length, -thickness / 2), hsize)
      val centersBounds = Array[Rect2D](top, right, bottom, left)

      val portBounds = (0 until 4).map(side => {
        val offset = thickness - numPorts * stride / 2 + portIdx * stride
        val rect = centersBounds(side)
        side match {
          case 0 | 2 => rect.mapX(_ + offset)
          case 1 | 3 => rect.mapY(_ + offset)
        }
      })

      (port, portBounds.toArray)
    }
  }

  // noinspection VarCouldBeVal
  var label: Option[String] = None

  override def minimumSize: Size2D = Size2D(68, 68)

  override def maximumSize: Size2D = minimumSize

  private def startMoving(pos: Vector2D): Unit = {
    highlight.goto(MovingHighlight)
    isMoving = true
    grabPoint = pos - position
  }

  private def move(pos: Vector2D): Unit = {
    val oldPos = position

    val desiredPos = if (Keyboard.isKeyDown(Keyboard.KEY_LCONTROL))
      (pos - workspaceView.cameraOffset).snap(68) + workspaceView.cameraOffset + Vector2D(34 - width / 2, 34 - height / 2)
    else
      pos - grabPoint

    position = desiredPos
    workspaceView.resolveCollision(this)

    if (workspaceView.collides(this) || (position - desiredPos).lengthSquared > 50 * 50)
      position = oldPos
  }

  private def stopMoving(): Unit = {
    isMoving = false
    if (isHovered)
      highlight.goto(HoveredHighlight)
    else
      highlight.goto(NoHighlight)

    workspaceView.resolveCollision(this)
  }

  def labelOrAddress: String = {
    label.orElse(Option(entity.node.address)).getOrElse("unknown")
  }

  override def draw(g: Graphics): Unit = {
    highlight.update()
    g.rect(position.x, position.y, size.width, size.height, highlight.color)
    g.sprite(icon, position.x + 2, position.y + 2, size.width - 4, size.height - 4, color = iconColor)
  }

  def drawLight(g: Graphics): Unit = {}

  def drawLabel(g: Graphics): Unit = {
    if (!exposeAddress) return

    g.setSmallFont()
    g.background = RGBAColor(0, 0, 0, 0)
    g.foreground = RGBAColor(150, 150, 150)
    DrawUtils.borderedText(g, position.x + 2, position.y - 10, labelOrAddress.take(8))
    g.setNormalFont()
  }

  def drawParticles(g: Graphics): Unit = {}

  def drawPorts(g: Graphics): Unit = {
    for ((port, rects) <- portsBounds) {
      val color = port.getColor
      for (rect <- rects)
        g.rect(rect, color)
    }
  }

  def window: Option[Window] = None

  def dispose(): Unit = {
    disconnectFromAll()
    window.foreach(_.hide())
    OcelotDesktop.workspace.remove(entity)
  }
}
