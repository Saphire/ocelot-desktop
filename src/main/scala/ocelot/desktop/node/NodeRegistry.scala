package ocelot.desktop.node

import ocelot.desktop.entity.{Camera, OpenFMRadio}
import ocelot.desktop.node.nodes._
import totoro.ocelot.brain.entity.{Cable, Case, ColorfulLamp, FloppyDiskDrive, IronNoteBlock, NoteBlock, Relay, Screen}

import scala.collection.mutable

object NodeRegistry {
  val types: mutable.ArrayBuffer[NodeType] = mutable.ArrayBuffer[NodeType]()

  private def register(t: NodeType): Unit = {
    types += t
  }

  for (tier <- 0 to 2) {
    register(NodeType("Screen" + tier, "nodes/Screen", tier) {
      new ScreenNode(new Screen(tier)).setup()
    })
  }

  register(NodeType("Disk Drive", "nodes/DiskDrive", -1) {
    new DiskDriveNode(new FloppyDiskDrive())
  })

  for (tier <- 0 to 3) {
    register(NodeType("Computer" + tier, "nodes/Computer", tier) {
      new ComputerNode(new Case(tier)).setup()
    })
  }

  register(NodeType("Relay", "nodes/Relay", -1) {
    new RelayNode(new Relay)
  })

  register(NodeType("Cable", "nodes/Cable", -1) {
    new CableNode(new Cable)
  })

  register(NodeType("NoteBlock", "nodes/NoteBlock", -1) {
    new NoteBlockNode(new NoteBlock)
  })

  register(NodeType("IronNoteBlock", "nodes/IronNoteBlock", -1) {
    new IronNoteBlockNode(new IronNoteBlock)
  })

  register(NodeType("Camera", "nodes/Camera", -1) {
    new CameraNode(new Camera)
  })

  register(NodeType("ColorfulLamp", "nodes/Lamp", -1) {
    new ColorfulLampNode(new ColorfulLamp)
  })

  register(NodeType("OpenFM radio", "nodes/OpenFMRadio", -1) {
    new OpenFMRadioNode(new OpenFMRadio)
  })
}
