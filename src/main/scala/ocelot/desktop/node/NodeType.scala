package ocelot.desktop.node

class NodeType(val name: String, val icon: String, val tier: Int, factory: => Node) extends Ordered[NodeType] {
  def make(): Node = factory

  override def compare(that: NodeType): Int = this.name.compare(that.name)
}

object NodeType {
  def apply(name: String, icon: String, tier: Int)(factory: => Node): NodeType = {
    new NodeType(name, icon, tier, factory)
  }
}
