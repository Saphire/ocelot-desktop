package ocelot.desktop.node

import ocelot.desktop.geometry.Size2D
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.event.handlers.{ClickHandler, HoverHandler}
import ocelot.desktop.ui.event.{ClickEvent, MouseEvent}
import ocelot.desktop.ui.widget.Widget
import ocelot.desktop.util.{Spritesheet, TierColor}

class NodeTypeWidget(val nodeType: NodeType) extends Widget with ClickHandler with HoverHandler {
  override def minimumSize: Size2D = Size2D(68, 68)
  override def maximumSize: Size2D = minimumSize

  size = maximumSize

  def onClick(): Unit = {}

  override def receiveMouseEvents = true

  eventHandlers += {
    case ClickEvent(MouseEvent.Button.Left, _) => onClick()
  }

  override def draw(g: Graphics): Unit = {
    val size = Spritesheet.spriteSize(nodeType.icon) * 4
    g.sprite(nodeType.icon, position.x + 34 - size.width / 2, position.y + 34 - size.height / 2, size.width, size.height, TierColor.get(nodeType.tier))
  }

  override def update(): Unit = {
    super.update()
    if (isHovered) {
      root.get.statusBar.addMouseEntry("icons/LMB", "Add node")
    }
  }
}
