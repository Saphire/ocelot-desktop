package ocelot.desktop.node.nodes

import ocelot.desktop.geometry.Size2D
import ocelot.desktop.node.Node
import totoro.ocelot.brain.entity.Cable

class CableNode(val cable: Cable) extends Node(cable) {
  override def icon: String = "nodes/Cable"

  override def minimumSize: Size2D = Size2D(36, 36)

  override protected val exposeAddress = false
}
