package ocelot.desktop.node.nodes

import ocelot.desktop.entity.Camera
import ocelot.desktop.node.Node
import ocelot.desktop.windows.CameraWindow

class CameraNode(val camera: Camera) extends Node(camera) {
  override def icon: String = "nodes/Camera"

  private var currentWindow: Option[CameraWindow] = None

  override def window: Option[CameraWindow] = {
    currentWindow = Option(currentWindow.getOrElse(new CameraWindow(this)))
    currentWindow
  }
}
