package ocelot.desktop.node.nodes

import totoro.ocelot.brain.entity.IronNoteBlock

class IronNoteBlockNode(val ironNoteBlock: IronNoteBlock) extends NoteBlockNodeBase(ironNoteBlock) {
  override def icon: String = "nodes/IronNoteBlock"
}
