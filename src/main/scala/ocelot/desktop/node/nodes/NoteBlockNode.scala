package ocelot.desktop.node.nodes

import ocelot.desktop.ui.widget.contextmenu.{ContextMenu, ContextMenuEntry, ContextMenuSubmenu}
import totoro.ocelot.brain.entity.NoteBlock

class NoteBlockNode(val noteBlock: NoteBlock) extends NoteBlockNodeBase(noteBlock) {
  override def icon: String = "nodes/NoteBlock"

  override def setupContextMenu(menu: ContextMenu): Unit = {
    menu.addEntry(new ContextMenuSubmenu("Instrument") {
      {
        val maxLen = NoteBlockNode.Instruments.map(_._2.length).max
        for ((instrument, name) <- NoteBlockNode.Instruments) {
          val dot = if (noteBlock.instrument == instrument) '•' else ' '
          addEntry(new ContextMenuEntry(name.padTo(maxLen, ' ') + dot, () => noteBlock.instrument = instrument))
        }
      }
    })

    menu.addSeparator()
    super.setupContextMenu(menu)
  }
}

object NoteBlockNode {
  val Pitches: Seq[Float] = List(
    0.500000f, 0.529732f, 0.561231f, 0.594604f, 0.629961f, 0.667420f, 0.707107f, 0.749154f, 0.793701f, 0.840896f, 0.890899f, 0.943874f,
    1.059463f, 1.122462f, 1.189207f, 1.259921f, 1.334840f, 1.414214f, 1.498307f, 1.587401f, 1.681793f, 1.781797f, 1.887749f, 2.000000f)

  private val Instruments = List(
    ("bass", "Bass (Wood)"),
    ("snare", "Snare Drum (Sand)"),
    ("hat", "Hi-hat (Glass)"),
    ("basedrum", "Bass Drum (Stone)"),
    ("bell", "Glockenspiel (Gold)"),
    ("flute", "Flute (Clay)"),
    ("chime", "Chimes (Packed Ice)"),
    ("guitar", "Guitar (Wool)"),
    ("xylophone", "Xylophone (Bone)"),
    ("iron_xylophone", "Vibraphone (Iron)"),
    ("cow_bell", "Cow Bell (Soul Sand)"),
    ("didgeridoo", "Didgeridoo (Pumpkin)"),
    ("bit", "Square Wave (Emerald)"),
    ("banjo", "Banjo (Hay Bale)"),
    ("pling", "Electric Piano (Glowstone)"),
    ("harp", "Harp"),
  )
}