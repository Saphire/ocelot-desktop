package ocelot.desktop.node.nodes

import ocelot.desktop.node.{Node, NodePort}
import totoro.ocelot.brain.entity.Relay
import totoro.ocelot.brain.network
import totoro.ocelot.brain.util.Direction

class RelayNode(val relay: Relay) extends Node(relay) {
  override val icon: String = "nodes/Relay"

  override def ports: Array[NodePort] = Array(
    NodePort(Some(Direction.North)),
    NodePort(Some(Direction.South)),
    NodePort(Some(Direction.East)),
    NodePort(Some(Direction.West)),
    NodePort(Some(Direction.Up)),
    NodePort(Some(Direction.Down)))

  override def getNodeByPort(port: NodePort): network.Node = relay.sidedNode(port.direction.get)

  override protected val exposeAddress = false

  override def shouldReceiveEventsFor(address: String): Boolean = {
    ports.exists(port => getNodeByPort(port).address == address)
  }
}
