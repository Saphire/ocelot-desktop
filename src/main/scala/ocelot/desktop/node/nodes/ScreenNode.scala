package ocelot.desktop.node.nodes

import ocelot.desktop.OcelotDesktop
import ocelot.desktop.color.Color
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.node.Node
import ocelot.desktop.ui.widget.contextmenu.{ContextMenu, ContextMenuEntry}
import ocelot.desktop.util.TierColor
import ocelot.desktop.windows.ScreenWindow
import totoro.ocelot.brain.entity.{Keyboard, Screen}
import totoro.ocelot.brain.nbt.NBTTagCompound

class ScreenNode(val screen: Screen) extends Node(screen) {
  private var keyboard: Option[Keyboard] = None

  override def load(nbt: NBTTagCompound): Unit = {
    super.load(nbt)
    if (nbt.hasKey("string")) {
      keyboard = OcelotDesktop.workspace.entityByAddress(nbt.getString("string")).map(_.asInstanceOf[Keyboard])
    }
  }

  override def save(nbt: NBTTagCompound): Unit = {
    super.save(nbt)
    if (keyboard.isDefined) {
      nbt.setString("keyboard", keyboard.get.node.address)
    }
  }

  override def dispose(): Unit = {
    super.dispose()
    if (keyboard.isDefined) {
      OcelotDesktop.workspace.remove(keyboard.get)
    }
  }

  def setup(): ScreenNode = {
    val kbd = new Keyboard
    OcelotDesktop.workspace.add(kbd)
    screen.connect(kbd)
    keyboard = Some(kbd)
    this
  }

  override def icon: String = "nodes/Screen"

  override def iconColor: Color = TierColor.get(screen.tier)

  override protected val canOpen = true

  override def setupContextMenu(menu: ContextMenu): Unit = {
    if (screen.getPowerState)
      menu.addEntry(new ContextMenuEntry("Turn off", () => screen.setPowerState(false)))
    else
      menu.addEntry(new ContextMenuEntry("Turn on", () => screen.setPowerState(true)))

    menu.addSeparator()
    super.setupContextMenu(menu)
  }

  override def draw(g: Graphics): Unit = {
    super.draw(g)

    if (screen.getPowerState)
      g.sprite("nodes/ScreenOnOverlay", position.x + 2, position.y + 2, size.width - 4, size.height - 4)
  }

  override lazy val window: Option[ScreenWindow] = Some(new ScreenWindow(this))
}
