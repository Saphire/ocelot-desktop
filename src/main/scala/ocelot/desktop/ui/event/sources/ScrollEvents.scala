package ocelot.desktop.ui.event.sources

import ocelot.desktop.ui.event.ScrollEvent

import scala.collection.mutable.ArrayBuffer

object ScrollEvents {
  protected[sources] val _events = new ArrayBuffer[ScrollEvent]

  def events: Iterator[ScrollEvent] = _events.iterator
}
