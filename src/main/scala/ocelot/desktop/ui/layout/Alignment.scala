package ocelot.desktop.ui.layout

object Alignment extends Enumeration {
  val Start, End, Center, Stretch = Value
}
