package ocelot.desktop.ui.layout

import ocelot.desktop.geometry.Padding2D
import ocelot.desktop.ui.widget.Widget

class PaddingLayout(widget: Widget, padding: Padding2D) extends Layout(widget) {
  override def recalculateBounds(): Unit = {
    val inner = widget.children(0)
    minimumSize = inner.minimumSize + padding.sizeAddition
    maximumSize = inner.maximumSize + padding.sizeAddition
    inner.recalculateBounds()
  }

  override def relayout(): Unit = {
    val inner = widget.children(0)
    inner.rawSetPosition(widget.position + padding.offset)
    inner.rawSetSize(widget.size - padding.sizeAddition)
    inner.relayout()
  }
}
