package ocelot.desktop.ui.swing

import buildinfo.BuildInfo

import ocelot.desktop.ui.swing.DialogDragListener

import java.awt.Window.Type
import java.awt.{Color, Dimension, Insets, Rectangle}
import java.time.Year
import javax.swing.border.{CompoundBorder, EmptyBorder}
import javax.swing.{JDialog, JLabel, JPanel, SwingConstants}

class SplashScreen extends JDialog {
  private val backgroundImage = new ImagePanel("/ocelot/desktop/images/splash/splash.png")

  private val imageWidth: Int = backgroundImage.image.getWidth()
  private val imageHeight: Int = backgroundImage.image.getHeight()

  setUndecorated(true)
  setType(Type.UTILITY)
  setSize(new Dimension(imageWidth, imageHeight))
  setBackground(new Color(0, 0, 0, 0))
  setAlwaysOnTop(true)
  setFocusable(false)
  setResizable(false)
  setLocationRelativeTo(null)
  setVisible(true)

  // Make splash screen draggable
  val dragListener: DialogDragListener = new DialogDragListener(this)
  addMouseListener(dragListener)
  addMouseMotionListener(dragListener)

  // Dialog children panel
  private val panel = new JPanel()
  panel.setBackground(new Color(0, 0, 0, 0))
  panel.setLayout(null)

  private def addLabel(rectangle: Rectangle, text: String, horizontalAlignment: Int = SwingConstants.LEFT, verticalAlignment: Int = SwingConstants.TOP): JLabel = {
    val label = new JLabel(text)
    label.setForeground(Color.white)
    label.setBounds(rectangle)
    label.setHorizontalAlignment(horizontalAlignment)
    label.setVerticalAlignment(verticalAlignment)

    panel.add(label)

    label
  }

  private def setLabelMargin(label: JLabel, margin: Insets): Unit = {
    label.setBorder(new CompoundBorder(label.getBorder, new EmptyBorder(margin)))
  }

  // Application version
  private val applicationVersion: JLabel = addLabel(
    new Rectangle(0, 0, imageWidth, imageHeight),
    s"Version: ${BuildInfo.version} (${BuildInfo.commit.take(7)})",
    SwingConstants.LEFT,
    SwingConstants.BOTTOM
  )

  setLabelMargin(applicationVersion, new Insets(0, 20, 20, 0))

  // Status
  private val status: JLabel = addLabel(new Rectangle(324, 390, 300, 100), "Starting Ocelot...")

  // Copyright
  private val copyright: JLabel = addLabel(
    new Rectangle(0, 0, imageWidth, imageHeight),
    s"© 2018 - ${Year.now.getValue} Ocelot Dev Team",
    SwingConstants.RIGHT,
    SwingConstants.BOTTOM
  )

  setLabelMargin(copyright, new Insets(0, 0, 20, 20))

  // Progress bar
  private val progressBar = new ColoredPanel(Color.white)
  panel.add(progressBar)

  // Background image
  backgroundImage.setBackground(new Color(0, 0, 0, 0))
  backgroundImage.setBounds(0, 0, imageWidth, imageHeight)
  panel.add(backgroundImage)
  backgroundImage.image.getWidth

  add(panel)

  def setProgress(percent: Float): Unit = {
    progressBar.setBounds(0, 440, (imageWidth * percent).toInt, 3)
  }

  def setStatus(text: String, percent: Float): Unit = {
    status.setText(text)
    setProgress(percent)
  }
}
