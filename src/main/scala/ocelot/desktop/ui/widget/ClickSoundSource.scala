package ocelot.desktop.ui.widget

import ocelot.desktop.audio.SoundSource

trait ClickSoundSource {
  protected def clickSoundSource: SoundSource
}
