package ocelot.desktop.ui.widget

import ocelot.desktop.ColorScheme
import ocelot.desktop.color.Color
import ocelot.desktop.geometry.Size2D
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.event.HoverEvent
import ocelot.desktop.ui.event.handlers.HoverHandler
import ocelot.desktop.ui.widget.tooltip.Tooltip

class Histogram extends Widget with HoverHandler {
  override def minimumSize: Size2D = Size2D(274, 70)
  override def maximumSize: Size2D = minimumSize

  var text = "N/A"
  var history: Array[Float] = Seq.fill(21)(0.0f).toArray

  override def receiveMouseEvents = true

  eventHandlers += {
    case HoverEvent(state) =>
      if (state == HoverEvent.State.Enter) onHoverEnter()
      else onHoverLeave()
  }

  private def drawBars(g: Graphics): Unit = {
    def drawBarSegment(i: Int, color: Color): Unit = {
      g.sprite("BarSegment", position.x, position.y + i * 6, 16, 4, color)
      g.sprite("BarSegment", position.x + 18, position.y + i * 6, 16, 4, color)
    }

    val ratio = history.last
    val fillBars = (ratio * 10).round
    val emptyBars = (9 - fillBars).max(0)

    for (i <- 0 until emptyBars) {
      drawBarSegment(i, ColorScheme("HistogramBarEmpty"))
    }
    for (i <- emptyBars + 1 until 10) {
      drawBarSegment(i, ColorScheme("HistogramBarFill"))
    }
    drawBarSegment(emptyBars, ColorScheme("HistogramBarTop"))
  }

  private def drawText(g: Graphics): Unit = {
    g.setSmallFont()
    g.text(position.x + 17 - text.length * 4, position.y + 62, text)
    g.setNormalFont()
  }

  private def drawHistogram(g: Graphics): Unit = {
    for (i <- 0 until 22) {
      g.rect(position.x + 41 + i * 11, position.y, 2, 57, ColorScheme("HistogramGrid"))
    }
    for (i <- 0 until 6) {
      g.rect(position.x + 41, position.y + i * 11, 233, 2, ColorScheme("HistogramGrid"))
    }

    for ((entry, i) <- history.zipWithIndex) {
      val width = if (i == 20) 13 else 11
      val height = (entry * 57).max(2)
      g.rect(position.x + 41 + i * 11, position.y + 57 - height, width, 2, ColorScheme("HistogramEdge"))
      g.rect(position.x + 41 + i * 11, position.y + 59 - height, width, height - 2, ColorScheme("HistogramFill"))
    }
  }

  protected val tooltip: Option[Tooltip] = None

  def onHoverEnter(): Unit = {
    if (tooltip.isDefined)
      root.get.tooltipPool.addTooltip(tooltip.get)
  }

  def onHoverLeave(): Unit = {
    if (tooltip.isDefined)
      root.get.tooltipPool.closeTooltip(tooltip.get)
  }

  override def draw(g: Graphics): Unit = {
    drawBars(g)
    drawText(g)
    drawHistogram(g)
  }
}
