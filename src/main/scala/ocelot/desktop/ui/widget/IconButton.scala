package ocelot.desktop.ui.widget

import ocelot.desktop.ColorScheme
import ocelot.desktop.audio.{SoundSource, SoundSources}
import ocelot.desktop.color.Color
import ocelot.desktop.geometry.Size2D
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.event.handlers.{ClickHandler, HoverHandler}
import ocelot.desktop.ui.event.{ClickEvent, HoverEvent, MouseEvent}
import ocelot.desktop.util.animation.{ColorAnimation, ValueAnimation}
import ocelot.desktop.util.{DrawUtils, Spritesheet}

class IconButton(
  releasedIcon: String, pressedIcon: String,
  releasedColor: Color = Color.White,
  pressedColor: Color = Color.White,
  sizeMultiplier: Float = 1,
  isSwitch: Boolean = false,
  drawBackground: Boolean = false,
  padding: Float = 0,
  tooltip: Option[String] = None
) extends Widget with ClickHandler with HoverHandler with ClickSoundSource {

  private val speed = 10f
  private val colorAnimation = new ColorAnimation(pressedColor, speed)
  private val alphaAnimation = new ValueAnimation(0f, speed)

  private var _isOn = false

  override def receiveMouseEvents = true

  eventHandlers += {
    case HoverEvent(state) =>
      if (state == HoverEvent.State.Enter) onHoverEnter()
      else onHoverLeave()

    case MouseEvent(MouseEvent.State.Press, MouseEvent.Button.Left) => {
      if (isSwitch) {
        if (isOn) {
          release()
        }
        else {
          press()
        }
      }
      else {
        press()
      }

      clickSoundSource.play()
    }

    case ClickEvent(MouseEvent.Button.Left, _) => {
      if (!isSwitch)
        release()
    }
  }

  def isOn: Boolean = _isOn

  def onPressed(): Unit = {}

  def onReleased(): Unit = {}

  def onHoverEnter(): Unit =
    if (labelTooltip.isDefined)
      root.get.tooltipPool.addTooltip(labelTooltip.get)

  def onHoverLeave(): Unit =
    if (labelTooltip.isDefined)
      root.get.tooltipPool.closeTooltip(labelTooltip.get)

  def playPressAnimation(): Unit = {
    colorAnimation.goto(pressedColor)
    alphaAnimation.goto(1f)
    _isOn = true
  }

  def playReleaseAnimation(): Unit = {
    colorAnimation.goto(releasedColor)
    alphaAnimation.goto(0f)
    _isOn = false
  }

  def press(): Unit = {
    playPressAnimation()
    onPressed()
  }

  def release(): Unit = {
    playReleaseAnimation()
    onReleased()
  }


  size = minimumSize

  private def releasedIconSize: Size2D = Spritesheet.spriteSize(releasedIcon) * sizeMultiplier

  private def pressedIconSize: Size2D = Spritesheet.spriteSize(pressedIcon) * sizeMultiplier

  override def minimumSize: Size2D = releasedIconSize.max(pressedIconSize) + (padding * 2.0f)
  override def maximumSize: Size2D = minimumSize

  private val labelTooltip = tooltip.map(label => new LabelTooltip(label))

  override def draw(g: Graphics): Unit = {
    if (drawBackground) {
      g.rect(bounds, ColorScheme("ButtonBackground"))
      DrawUtils.ring(g, position.x, position.y, width, height, 2, ColorScheme("ButtonBorder"))
    }

    if (alphaAnimation.value < 1f)
      g.sprite(releasedIcon, position + (size - releasedIconSize) / 2f, releasedIconSize,
        releasedColor.toRGBANorm.mapA(_ * (1f - alphaAnimation.value)))

    if (alphaAnimation.value > 0f)
      g.sprite(pressedIcon, position + (size - pressedIconSize) / 2f, pressedIconSize,
        pressedColor.toRGBANorm.mapA(_ * alphaAnimation.value))
  }

  override def update(): Unit = {
    super.update()
    colorAnimation.update()
    alphaAnimation.update()
    if (isOn && !_isOn) playPressAnimation()
    if (!isOn && _isOn) playReleaseAnimation()
  }

  override protected def clickSoundSource: SoundSource = SoundSources.InterfaceClick
}
