package ocelot.desktop.ui.widget

import ocelot.desktop.audio.SoundSources
import ocelot.desktop.geometry.{Padding2D, Size2D}
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.event.KeyEvent
import ocelot.desktop.ui.widget.contextmenu.{ContextMenuEntry, ContextMenuSubmenu}
import ocelot.desktop.{ColorScheme, OcelotDesktop}
import org.lwjgl.input.Keyboard

class MenuBar extends Widget {
  override def receiveMouseEvents: Boolean = true

  private val entries = new Widget {}

  children :+= new PaddingBox(entries, Padding2D(left = 1, right = 1, bottom = 1))

  private def addEntry(w: Widget): Unit = entries.children :+= w

  addEntry(new MenuBarSubmenu("File", menu => {
    menu.addEntry(new ContextMenuEntry("New", () => OcelotDesktop.newWorkspace()))
    menu.addEntry(new ContextMenuEntry("Open", () => OcelotDesktop.open()))
    menu.addEntry(new ContextMenuEntry("Save", () => OcelotDesktop.save()))
    menu.addEntry(new ContextMenuEntry("Save as…", () => OcelotDesktop.saveAs()))
    menu.addSeparator()
    menu.addEntry(new ContextMenuEntry("Exit", () => OcelotDesktop.exit()))
  }))

  addEntry(new MenuBarSubmenu("Player", menu => {
    menu.addEntry(new ContextMenuEntry("Add...", () => OcelotDesktop.addPlayerDialog()))
    menu.addSeparator()
    OcelotDesktop.players.foreach(player => {
      menu.addEntry(new ContextMenuSubmenu(
          (if (player == OcelotDesktop.players.head) "● " else "  ") + player.nickname,
          () => OcelotDesktop.selectPlayer(player.nickname)
      ) {
        addEntry(new ContextMenuEntry(
          "Remove",
          () => OcelotDesktop.removePlayer(player.nickname),
          sound = SoundSources.InterfaceClickLow
        ))
      })
    })
  }))

  addEntry(new MenuBarButton("Settings", () => OcelotDesktop.settings()))

  addEntry(new Widget {
    override def maximumSize: Size2D = Size2D(Float.PositiveInfinity, 1)
  }) // fill remaining space

  eventHandlers += {
    case KeyEvent(KeyEvent.State.Press, Keyboard.KEY_F5, _) => OcelotDesktop.save()
    case KeyEvent(KeyEvent.State.Press, Keyboard.KEY_F9, _) => OcelotDesktop.open()
  }

  override def draw(g: Graphics): Unit = {
    g.rect(bounds, ColorScheme("TitleBarBackground"))
    drawChildren(g)
    g.rect(bounds.x, bounds.y + height - 1, width, height, ColorScheme("TitleBarBorder"))
  }
}
