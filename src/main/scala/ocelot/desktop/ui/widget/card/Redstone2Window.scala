package ocelot.desktop.ui.widget.card

import ocelot.desktop.geometry.{Padding2D, Size2D}
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.layout.{Alignment, Layout, LinearLayout}
import ocelot.desktop.ui.widget.window.BasicWindow
import ocelot.desktop.ui.widget.{Knob, Label, PaddingBox, Widget}
import ocelot.desktop.util.{DrawUtils, Orientation}
import totoro.ocelot.brain.entity.Redstone
import totoro.ocelot.brain.util.DyeColor

class Redstone2Window(card: Redstone.Tier2) extends BasicWindow {
  private def bundledKnob(side: Int, col: Int) = new Knob(DyeColor.byCode(col)) {
    override def input: Int = {
      card.bundledRedstoneInput(side)(col) min 15 max 0
    }

    override def input_=(v: Int): Unit = {
      card.bundledRedstoneInput(side)(col) = v
    }

    override def output: Int = card.bundledRedstoneOutput(side)(col) min 15 max 0
  }

  private def bundledBlock(side: Int, name: String) = new PaddingBox(new Widget {
    override protected val layout: Layout = new LinearLayout(this) {
      orientation = Orientation.Vertical
      contentAlignment = Alignment.Center
    }

    children :+= new Label {
      override def text: String = name
      override def maximumSize: Size2D = minimumSize
    }
    children :+= new Widget {
      children :+= bundledKnob(side, 0)
      children :+= bundledKnob(side, 1)
      children :+= bundledKnob(side, 2)
      children :+= bundledKnob(side, 3)
    }
    children :+= new Widget {
      children :+= bundledKnob(side, 4)
      children :+= bundledKnob(side, 5)
      children :+= bundledKnob(side, 6)
      children :+= bundledKnob(side, 7)
    }
    children :+= new Widget {
      children :+= bundledKnob(side, 8)
      children :+= bundledKnob(side, 9)
      children :+= bundledKnob(side, 10)
      children :+= bundledKnob(side, 11)
    }
    children :+= new Widget {
      children :+= bundledKnob(side, 12)
      children :+= bundledKnob(side, 13)
      children :+= bundledKnob(side, 14)
      children :+= bundledKnob(side, 15)
    }
  }, Padding2D.equal(4))

  children :+= new PaddingBox(new Widget {
    override protected val layout: Layout = new LinearLayout(this, orientation = Orientation.Vertical)

    children :+= new PaddingBox(new Label {
      override def text: String = s"Bundled I/O — ${card.node.address.substring(0, 24)}…"
      override val isSmall: Boolean = true
    }, Padding2D.equal(4))

    children :+= new Widget {
      children :+= new Widget {
        override protected val layout: Layout = new LinearLayout(this, orientation = Orientation.Vertical)
        children :+= bundledBlock(0, "Bottom")
        children :+= bundledBlock(1, "Top")
      }
      children :+= new Widget {
        override protected val layout: Layout = new LinearLayout(this, orientation = Orientation.Vertical)
        children :+= bundledBlock(2, "Back")
        children :+= bundledBlock(3, "Front")
      }
      children :+= new Widget {
        override protected val layout: Layout = new LinearLayout(this, orientation = Orientation.Vertical)
        children :+= bundledBlock(4, "Right")
        children :+= bundledBlock(5, "Left")
      }
    }
  }, Padding2D.equal(8))

  override def draw(g: Graphics): Unit = {
    beginDraw(g)
    DrawUtils.windowWithShadow(g, position.x, position.y, size.width, size.height, 1f, 0.5f)
    drawChildren(g)
    endDraw(g)
  }
}
