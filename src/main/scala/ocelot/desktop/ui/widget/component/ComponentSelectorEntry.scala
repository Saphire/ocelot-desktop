package ocelot.desktop.ui.widget.component

import ocelot.desktop.ColorScheme
import ocelot.desktop.color.Color
import ocelot.desktop.geometry.Padding2D
import ocelot.desktop.graphics.IconDef
import ocelot.desktop.ui.layout.{Alignment, Layout, LinearLayout}
import ocelot.desktop.ui.widget.{Icon, Label, PaddingBox, Widget}

class ComponentSelectorEntry(label: String, icon: IconDef) extends Widget {
  children :+= new Widget {
    override val layout: Layout = new LinearLayout(this) {
      contentAlignment = Alignment.Center
    }

    children :+= new PaddingBox(new Icon(icon), Padding2D(left = 4f, top = 2f, bottom = 2f))

    children :+= new PaddingBox(new Label {
      override def text: String = label
      override def color: Color = ColorScheme("ComponentSelectorText")
    }, Padding2D(left = 8f, right = 8f))
  }

  override def receiveMouseEvents: Boolean = true
}
