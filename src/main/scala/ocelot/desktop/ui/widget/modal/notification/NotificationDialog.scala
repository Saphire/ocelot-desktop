package ocelot.desktop.ui.widget.modal.notification

import ocelot.desktop.ColorScheme
import ocelot.desktop.color.Color
import ocelot.desktop.geometry.Padding2D
import ocelot.desktop.graphics.{Graphics, IconDef}
import ocelot.desktop.ui.layout.LinearLayout
import ocelot.desktop.ui.widget._
import ocelot.desktop.ui.widget.modal.ModalDialog
import ocelot.desktop.ui.widget.modal.notification.NotificationType.NotificationType
import ocelot.desktop.util.{DrawUtils, Orientation}

class NotificationDialog(message: String, notificationType: NotificationType = NotificationType.Warning) extends ModalDialog {
  protected val buttonsLayout: Widget = new Widget {
    override val layout: LinearLayout = new LinearLayout(this) {
      spacing = 15
    }

    children :+= new Filler
  }

  children :+= new Widget {
    override val layout = new LinearLayout(this)

    children :+= new Widget {
      override def draw(g: Graphics): Unit = {
        // Background color
        g.rect(bounds, ColorScheme(s"Notification${notificationType.toString}"))

        super.draw(g)
      }

      // Icon
      children :+= new PaddingBox(
        new Icon(new IconDef(s"icons/Notification${notificationType.toString}", 4)),
        Padding2D.equal(10)
      )

      children :+= new Filler
    }
  }

  children :+= new PaddingBox(
    new Widget {
      override val layout: LinearLayout = new LinearLayout(this, orientation = Orientation.Vertical, 15f)

      // Text
      children :+= new Widget {
        override val layout: LinearLayout = new LinearLayout(this, orientation = Orientation.Vertical, 5f)

        children :++= message.split('\n').map(line => new Label {
          override def text: String = line

          override def color: Color = ColorScheme("ContextMenuText")
        })
      }

      // Buttons
      children :+= new Widget {
        children :+= new Filler

        children :+= buttonsLayout
      }
    },
    Padding2D.equal(15)
  )

  def onSaveSelected(): Unit = {}

  def onExitSelected(): Unit = {}

  protected override def drawInner(g: Graphics): Unit = {
    DrawUtils.shadow(g, bounds.x - 8, bounds.y - 8, bounds.w + 16, bounds.h + 20, 0.5f)
    g.rect(bounds, ColorScheme("ContextMenuBackground"))
    DrawUtils.ring(g, bounds.x, bounds.y, bounds.w, bounds.h, 1, ColorScheme("ContextMenuBorder"))

    drawChildren(g)
  }

  def addButton(buttonText: String, buttonOnClick: () => Unit): NotificationDialog = {
    buttonsLayout.children :+= new Button {
      override def text: String = buttonText

      override def onClick(): Unit = buttonOnClick()
    }

    this
  }

  def addCloseButton(buttonText: String = "Ok"): NotificationDialog = addButton(buttonText, close)

  override def show(): Unit = {
    relayout()

    super.show()
  }
}
