package ocelot.desktop.ui.widget.modal.notification

object NotificationType extends Enumeration {
  type NotificationType = Value

  val Error, Warning, Info = Value
}