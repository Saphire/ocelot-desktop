package ocelot.desktop.ui.widget.settings

import ocelot.desktop.Settings
import ocelot.desktop.geometry.{Padding2D, Size2D}
import ocelot.desktop.ui.widget.{PaddingBox, Slider}

class SoundSettingsTab extends SettingsTab {
  override val icon: String = "icons/SettingsSound"
  override val label: String = "Sound"

  children :+= new PaddingBox(new Slider(Settings.get.volumeMaster, "Master Volume") {
    override def minimumSize: Size2D = Size2D(512, 24)

    override def onValueChanged(value: Float): Unit = {
      Settings.get.volumeMaster = value
      applySettings()
    }
  }, Padding2D(bottom = 8))

  children :+= new PaddingBox(new Slider(Settings.get.volumeBeep, "Beep Volume") {
    override def minimumSize: Size2D = Size2D(512, 24)

    override def onValueChanged(value: Float): Unit = {
      Settings.get.volumeBeep = value
    }
  }, Padding2D(bottom = 8))

  children :+= new PaddingBox(new Slider(Settings.get.volumeEnvironment, "Environment Volume") {
    override def minimumSize: Size2D = Size2D(512, 24)

    override def onValueChanged(value: Float): Unit = {
      Settings.get.volumeEnvironment = value
      applySettings()
    }
  }, Padding2D(bottom = 8))

  children :+= new PaddingBox(new Slider(Settings.get.volumeInterface, "Interface Volume") {
    override def minimumSize: Size2D = Size2D(512, 24)

    override def onValueChanged(value: Float): Unit = {
      Settings.get.volumeInterface = value
      applySettings()
    }
  }, Padding2D(bottom = 8))
}
