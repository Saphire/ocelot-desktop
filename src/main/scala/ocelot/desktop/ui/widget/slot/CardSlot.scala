package ocelot.desktop.ui.widget.slot

import ocelot.desktop.color.Color
import ocelot.desktop.graphics.IconDef
import ocelot.desktop.ui.UiHandler
import ocelot.desktop.ui.widget.{Label, TunnelDialog, Widget}
import ocelot.desktop.ui.widget.card.{Redstone1Window, Redstone2Window, SoundCardWindow}
import ocelot.desktop.ui.widget.contextmenu.{ContextMenu, ContextMenuEntry, ContextMenuSubmenu}
import totoro.ocelot.brain.entity.sound_card.SoundCard
import totoro.ocelot.brain.entity.{LinkedCard, Redstone}
import totoro.ocelot.brain.entity.traits.{Entity, Inventory}
import totoro.ocelot.brain.util.Tier

class CardSlot(owner: Inventory#Slot, val tier: Int) extends InventorySlot[Entity](owner) {
  override def itemIcon: Option[IconDef] = item.flatMap(it => CardRegistry.getIcon(it))

  override def icon: IconDef = new IconDef("icons/Card")
  override def tierIcon: Option[IconDef] = Some(new IconDef("icons/Tier" + tier))

  override def fillRmbMenu(menu: ContextMenu): Unit = {
    val pool = UiHandler.root.workspaceView.windowPool
    item match {
      case Some(card: Redstone.Tier2) =>
        menu.addEntry(new ContextMenuEntry("Redstone I/O", () => pool.openWindow(new Redstone1Window(card))))
        menu.addEntry(new ContextMenuEntry("Bundled I/O", () => pool.openWindow(new Redstone2Window(card))))
      case Some(card: Redstone.Tier1) =>
        menu.addEntry(new ContextMenuEntry("Redstone I/O", () => pool.openWindow(new Redstone1Window(card))))
      case Some(card: LinkedCard) =>
        menu.addEntry(new ContextMenuEntry("Set channel", () => new TunnelDialog(
          tunnel => card.tunnel = tunnel,
          card.tunnel
        ).show()))
      case Some(card: SoundCard) =>
        menu.addEntry(new ContextMenuEntry("Open", () => pool.openWindow(new SoundCardWindow(card))))
      case _ =>
    }
    super.fillRmbMenu(menu)
  }

  override def fillLmbMenu(menu: ContextMenu): Unit = {
    val entries = CardRegistry.entries

    var i = 0
    while (i < entries.length) {
      val entry = entries(i)
      val nextEntry = entries.lift(i + 1)

      if (nextEntry.isDefined && nextEntry.get.name == entry.name) {
        val groupName = entry.name
        if (tier >= entry.tier) {
          menu.addEntry(new ContextMenuSubmenu(groupName, icon = Some(nextEntry.get.icon)) {
            entries.view.slice(i, entries.length).takeWhile(_.name == groupName).foreach(entry => {
              val name = if (entry.tier == Tier.Four) "Creative" else "Tier " + (entry.tier + 1)
              if (tier >= entry.tier) {
                addEntry(new ContextMenuEntry(name, () => item = entry.factory(), Some(entry.icon)))
              }
              i += 1
            })
          })
        }
      } else {
        if (tier >= entry.tier) {
          menu.addEntry(new ContextMenuEntry(entry.name, () => item = entry.factory(), Some(entry.icon)))
        }

        i += 1
      }
    }
  }

  override def lmbMenuEnabled: Boolean = true

  override def tooltipChildrenAdder(inner: Widget): Unit = {
    super.tooltipChildrenAdder(inner)

    item match {
      case Some(card: LinkedCard) =>
        inner.children :+= new Label {
          override def text: String = s"Channel: ${card.tunnel}"
          override def color: Color = Color.Grey
        }
      case _ =>
    }
  }
}
