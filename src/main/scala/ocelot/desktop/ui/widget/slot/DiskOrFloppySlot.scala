package ocelot.desktop.ui.widget.slot

import ocelot.desktop.OcelotDesktop
import ocelot.desktop.ui.widget.contextmenu.{ContextMenu, ContextMenuEntry}
import totoro.ocelot.brain.entity.traits.{DiskManaged, Entity}

import javax.swing.JFileChooser
import scala.util.Try

trait DiskOrFloppySlot {
  def addSetDirectoryEntryToRmbMenu(menu: ContextMenu, slot: InventorySlot[DiskManaged with Entity]): Unit = {
    menu.addEntry(new ContextMenuEntry(
      "Set directory",
      () => OcelotDesktop.showFileChooserDialog(
        dir => Try {
          if (dir.isDefined && slot.item.isDefined) {
            val item = slot.item
            // first - unload the item with old filesystem (triggers 'component_removed' signal)
            slot.item = None
            // then - trigger filesystem rebuild with a new path
            item.get.customRealPath = Some(dir.get.toPath.toAbsolutePath)
            // finally - add the item back (triggers 'component_added' signal)
            slot.item = item
          }
        },
        JFileChooser.OPEN_DIALOG,
        JFileChooser.DIRECTORIES_ONLY
      )
    ))
  }
}
