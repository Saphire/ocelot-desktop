package ocelot.desktop.ui.widget.slot

import ocelot.desktop.OcelotDesktop.showFileChooserDialog
import ocelot.desktop.color.Color
import ocelot.desktop.graphics.IconDef
import ocelot.desktop.ui.widget.contextmenu.{ContextMenu, ContextMenuEntry, ContextMenuSubmenu}
import ocelot.desktop.ui.widget.{InputDialog, Label, Widget}
import totoro.ocelot.brain.entity.EEPROM
import totoro.ocelot.brain.entity.traits.Inventory
import totoro.ocelot.brain.loot.Loot

import java.net.{MalformedURLException, URL}
import javax.swing.JFileChooser
import scala.util.Try

class EEPROMSlot(owner: Inventory#Slot) extends InventorySlot[EEPROM](owner) {
  override def itemIcon: Option[IconDef] = item.map(_ => new IconDef("items/EEPROM"))
  override def icon: IconDef = new IconDef("icons/EEPROM")

  override def lmbMenuEnabled: Boolean = true

  override def fillLmbMenu(menu: ContextMenu): Unit = {
    menu.addEntry(new ContextMenuEntry("Lua BIOS",
      () => item = Loot.LuaBiosEEPROM.create(),
      icon = Some(new IconDef("items/EEPROM"))
    ))

    menu.addEntry(new ContextMenuEntry("AdvLoader",
      () => item = Loot.AdvLoaderEEPROM.create(),
      icon = Some(new IconDef("items/EEPROM"))
    ))

    menu.addEntry(new ContextMenuEntry("Empty",
      () => item = new EEPROM,
      icon = Some(new IconDef("items/EEPROM"))
    ))
  }

  override def fillRmbMenu(menu: ContextMenu): Unit = {
    val dataSourceMenu = new ContextMenuSubmenu("External data source")

    dataSourceMenu.addEntry(new ContextMenuEntry("Local file", () => showFileChooserDialog(
      file => Try {
        if (file.isDefined)
          item.get.codePath = Some(file.get.toPath)
      },
      JFileChooser.OPEN_DIALOG,
      JFileChooser.FILES_ONLY
    )))

    dataSourceMenu.addEntry(new ContextMenuEntry("File via URL", () =>
      new InputDialog(
        title = "File via URL",
        text => item.get.codeURL = Some(new URL(text)),
        "",
        text => {
          try {
            new URL(text)
            true
          }
          catch {
            case _: MalformedURLException => false
          }
        }
      ).show()
    ))

    if (item.get.codePath.isDefined || item.get.codeURL.isDefined)
      dataSourceMenu.addEntry(new ContextMenuEntry("Detach", () => item.get.codeBytes = Some(Array.empty[Byte])))

    menu.addEntry(dataSourceMenu)

    super.fillRmbMenu(menu)
  }

  override def tooltipLine1Text: String = getTooltipSuffixedText(getTooltipDeviceInfoText(item.get), item.get.label)

  override def tooltipChildrenAdder(inner: Widget): Unit = {
    super.tooltipChildrenAdder(inner)

    if (item.get.codePath.isEmpty && item.get.codeURL.isEmpty)
      return

    inner.children :+= new Label {
      override def text: String = {
        if (item.isDefined) {
          if (item.get.codePath.isDefined) s"Source path: ${item.get.codePath.get.toString}"
          else if (item.get.codeURL.isDefined) s"Source URL: ${item.get.codeURL.get.toString}"
          else ""
        } else ""
      }

      override def color: Color = Color.Grey
    }
  }
}
