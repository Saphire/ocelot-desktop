package ocelot.desktop.ui.widget.slot

import ocelot.desktop.color.Color
import ocelot.desktop.graphics.IconDef
import ocelot.desktop.ui.widget.contextmenu.{ContextMenu, ContextMenuEntry}
import totoro.ocelot.brain.entity.Memory
import totoro.ocelot.brain.entity.traits.{Inventory, MultiTiered}
import totoro.ocelot.brain.util.Tier

class MemorySlot(owner: Inventory#Slot, val tier: Int) extends InventorySlot[Memory](owner) {
  override def itemIcon: Option[IconDef] = item.map(mem => new IconDef("items/Memory" + mem.tier))

  override def icon: IconDef = new IconDef("icons/Memory")
  override def tierIcon: Option[IconDef] = Some(new IconDef("icons/Tier" + tier))

  override def fillLmbMenu(menu: ContextMenu): Unit = {
    menu.addEntry(new ContextMenuEntry("RAM (Tier 1)",
      () => item = new Memory(Tier.One),
      icon = Some(new IconDef("items/Memory0"))))

    menu.addEntry(new ContextMenuEntry("RAM (Tier 1.5)",
      () => item = new Memory(Tier.Two),
      icon = Some(new IconDef("items/Memory1"))))

    if (tier >= Tier.Two) {
      menu.addEntry(new ContextMenuEntry("RAM (Tier 2)",
        () => item = new Memory(Tier.Three),
        icon = Some(new IconDef("items/Memory2"))))

      menu.addEntry(new ContextMenuEntry("RAM (Tier 2.5)",
        () => item = new Memory(Tier.Four),
        icon = Some(new IconDef("items/Memory3"))))
    }

    if (tier >= Tier.Three) {
      menu.addEntry(new ContextMenuEntry("RAM (Tier 3)",
        () => item = new Memory(Tier.Five),
        icon = Some(new IconDef("items/Memory4"))))

      menu.addEntry(new ContextMenuEntry("RAM (Tier 3.5)",
        () => item = new Memory(Tier.Six),
        icon = Some(new IconDef("items/Memory5"))))
    }
  }

  override def lmbMenuEnabled: Boolean = true

  override def tooltipLine1Color: Color = item.get match {
    case tiered: MultiTiered => tiered.tier match {
      case 0 => Color.White
      case 1 => Color.White
      case 2 => Color.Yellow
      case 3 => Color.Yellow
      case 4 => Color.Cyan
      case _ => Color.Cyan
    }
    case _ => Color.White
  }

  override def tooltipLine1Text: String = {
    val tier = (item.get.tier + 2) / 2f
    val intTier = tier.toInt

    getTooltipDeviceInfoText(item.get, getTooltipTieredText(if (tier - intTier > 0) tier.toString else intTier.toString))
  }
}
