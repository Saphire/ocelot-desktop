package ocelot.desktop.ui.widget.statusbar

import ocelot.desktop.ColorScheme
import ocelot.desktop.color.Color
import ocelot.desktop.geometry.Size2D
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.widget.Widget
import ocelot.desktop.util.{DrawUtils, Spritesheet}

class KeyMouseEntry(val icon: String, val key: String, val text: String) extends Widget {
  private val iconSize = Spritesheet.spriteSize(icon)

  override def minimumSize: Size2D = Size2D(iconSize.width + key.length * 8 + 40 + text.length * 8, 16)
  override def maximumSize: Size2D = minimumSize

  override def draw(g: Graphics): Unit = {
    g.sprite(icon, position + Size2D(8, height / 2 - iconSize.height / 2), ColorScheme("StatusBarIcon"))

    g.background = Color.Transparent
    g.foreground = ColorScheme("StatusBarKey")
    g.setSmallFont()
    DrawUtils.borderedText(g, position.x + iconSize.width + 8, position.y + 5, "+" + key)

    g.foreground = ColorScheme("StatusBarForeground")
    g.setNormalFont()
    g.text(position.x + iconSize.width + key.length * 8 + 24, position.y, text)
  }
}
