package ocelot.desktop.ui.widget.verticalmenu

import ocelot.desktop.ColorScheme
import ocelot.desktop.geometry.Size2D
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.ui.widget.Widget

class VerticalMenuFiller extends Widget {
  override def maximumSize: Size2D = Size2D.Inf

  override def draw(g: Graphics): Unit = {
    g.rect(bounds.x + bounds.w - 2f, bounds.y, 2f, bounds.h, ColorScheme("VerticalMenuBorder"))
  }
}
