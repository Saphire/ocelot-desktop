package ocelot.desktop.ui.widget.window

import ocelot.desktop.ColorScheme
import ocelot.desktop.color.{Color, RGBAColorNorm}
import ocelot.desktop.geometry.{Padding2D, Rect2D}
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.node.{NodeRegistry, NodeTypeWidget}
import ocelot.desktop.ui.UiHandler
import ocelot.desktop.ui.layout.LinearLayout
import ocelot.desktop.ui.widget.{PaddingBox, ScrollView, Widget, WorkspaceView}
import ocelot.desktop.util.animation.{ColorAnimation, UnitAnimation}
import ocelot.desktop.util.{DrawUtils, Logging, Orientation}

class NodeSelector extends Window with Logging {
  override protected val layout = new LinearLayout(this)

  private val rowsWidget: Widget = new Widget {
    override protected val layout = new LinearLayout(this, Orientation.Vertical)
  }

  private val inner: Widget = new PaddingBox(rowsWidget, Padding2D.equal(4))
  private val scrollView = new ScrollView(inner)
  private val outer: Widget = new PaddingBox(scrollView, Padding2D.equal(4))
  children +:= outer

  private var numNodes: Int = 0

  private var _isShown: Boolean = false
  private var isClosing: Boolean = false
  private var heightAnimation: UnitAnimation = _
  private var animationTime: Float = 0f

  private val FinalRingColor: RGBAColorNorm = ColorScheme("NodeSelectorRing")
  private val colorAnimation: ColorAnimation = new ColorAnimation(FinalRingColor.withAlpha(0f), speed = 3f)

  initNodeTypes()

  override protected def dragRegions: Iterator[Rect2D] = Iterator()

  private def workspaceView: WorkspaceView = windowPool.parent.get.asInstanceOf[WorkspaceView]

  private def rows: Array[Widget] = rowsWidget.children

  // noinspection ScalaUnusedSymbol
  private def rows_=(rows: Array[Widget]): Unit = rowsWidget.children = rows

  private def addNodeWidget(widget: Widget): Unit = {
    val row = if (rows.lastOption.exists(row => row.children.length < 4)) {
      rows.last
    } else {
      val row = new Row(rows.length)
      rows :+= row
      row
    }

    row.children :+= widget
    numNodes += 1
  }

  private def initNodeTypes(): Unit = {
    NodeRegistry.types.foreach(t => addNodeWidget(new NodeTypeWidget(t) {
      override def onClick(): Unit = {
        workspaceView.addNode(nodeType.make())
        hide()
      }
    }))
  }

  size = maximumSize

  def finalHeight: Float = (rows.length * 68f + 16).min(250f)

  def isShown: Boolean = _isShown

  def ringColor: Color = colorAnimation.color

  override def update(): Unit = {
    if (isShown) {
      colorAnimation.update()
      heightAnimation.update()

      if (isClosing) {
        animationTime -= UiHandler.dt
      } else {
        animationTime += UiHandler.dt
      }

      width = inner.maximumSize.width + 8
      height = heightAnimation.value * finalHeight

      if (isClosing && size.height < 10) {
        _isOpen = false
        _isFocused = false
        _isShown = false
        isClosing = false
        workspaceView.windowPool.closeWindow(this)
      }
    }

    super.update()
  }

  override def draw(g: Graphics): Unit = {
    if (!isShown) return

    g.rect(position.x, position.y, size.width, size.height,
      ColorScheme("NodeSelectorBackground").mapA(_ * colorAnimation.color.toRGBANorm.a))
    DrawUtils.ring(g, position.x, position.y, size.width, size.height, thickness = 1, color = colorAnimation.color)

    super.draw(g)
  }

  override def show(): Unit = {
    if (isClosing) return

    colorAnimation.goto(FinalRingColor)

    _isOpen = true
    _isFocused = true
    _isShown = true
    heightAnimation = UnitAnimation.easeOutQuad(0.25f)
    heightAnimation.goUp()
    animationTime = 0.016f * 4f * scrollView.offset.y / 68f
  }

  override def hide(): Unit = {
    if (isClosing) return

    colorAnimation.goto(FinalRingColor.copy(a = 0f))

    heightAnimation = UnitAnimation.easeInQuad(0.25f)
    heightAnimation.time = 1
    heightAnimation.goDown()
    animationTime = 0.25f + 0.016f * 4f * scrollView.offset.y / 68f
    isClosing = true
  }

  private class Row(rowIdx: Int) extends Widget {
    override protected val layout = new LinearLayout(this, Orientation.Horizontal)

    override protected def drawChildren(g: Graphics): Unit = {
      if (!isShown) return

      for ((child, i) <- children.zipWithIndex) {
        val idx = rowIdx * 4 + i
        val timeOffset = idx.toFloat * 0.016f

        g.save()
        val sx = child.position.x + child.size.width / 2
        val sy = child.position.y + child.size.height / 2
        g.translate(sx, sy)
        g.scale(((animationTime - timeOffset) / 0.1f).max(0f).min(1f))
        g.translate(-sx, -sy)
        child.draw(g)
        g.restore()
      }
    }
  }

}