package ocelot.desktop.util

import scala.collection.mutable.ArrayBuffer

object ResourceManager {
  private var initialized = false
  private val resources = new ArrayBuffer[Resource]

  def registerResource(resource: Resource): Unit = {
    resources += resource
    if (initialized) resource.initResource()
  }

  def unregisterResource(resource: Resource): Unit = {
    resources -= resource
  }

  def initResources(): Unit = {
    if (!initialized) {
      forEach(_.initResource())
      initialized = true
    }
  }

  def forEach(predicate: Resource => Unit): Unit = {
    resources.foreach(predicate)
  }

  def freeResource(resource: Resource): Unit = {
    resource.freeResource()
    unregisterResource(resource)
  }

  def freeResources(): Unit = {
    forEach(_.freeResource())
    resources.clear()
  }
}
