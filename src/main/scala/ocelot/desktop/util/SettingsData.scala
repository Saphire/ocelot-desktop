package ocelot.desktop.util

import ocelot.desktop.Settings.Int2D

class SettingsData {
  def this(data: SettingsData) {
    this()
    updateWith(data)
  }

  var volumeMaster: Float = 1f
  var volumeBeep: Float = 1f
  var volumeEnvironment: Float = 1f
  var volumeInterface: Float = 0.5f

  var windowSize: Int2D = new Int2D()
  var windowPosition: Int2D = new Int2D()
  var windowValidatePosition: Boolean = true
  var windowFullscreen: Boolean = false

  var recentWorkspace: Option[String] = None

  var stickyWindows: Boolean = true
  var saveOnExit: Boolean = true
  var openLastWorkspace: Boolean = true

  def updateWith(data: SettingsData): Unit = {
    // TODO: maybe apply some automated mapping solution
    // TODO: please do that ☝
    this.volumeMaster = data.volumeMaster
    this.volumeBeep = data.volumeBeep
    this.volumeEnvironment = data.volumeEnvironment
    this.volumeInterface = data.volumeInterface

    this.windowPosition = data.windowPosition
    this.windowValidatePosition = data.windowValidatePosition
    this.windowSize = data.windowSize
    this.windowFullscreen = data.windowFullscreen

    this.stickyWindows = data.stickyWindows
    this.saveOnExit = data.saveOnExit
    this.openLastWorkspace = data.openLastWorkspace
  }
}
