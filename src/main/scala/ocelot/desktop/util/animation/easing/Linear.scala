package ocelot.desktop.util.animation.easing

object Linear extends EasingFunction {
  override def derivative(t: Float): Float = 1f

  override def apply(t: Float): Float = t
}
