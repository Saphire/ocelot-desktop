package ocelot.desktop.windows

import com.github.sarxos.webcam.Webcam
import ocelot.desktop.entity.Camera
import ocelot.desktop.geometry.{Padding2D, Size2D, Vector2D}
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.node.nodes.CameraNode
import ocelot.desktop.ui.layout.{Layout, LinearLayout}
import ocelot.desktop.ui.widget.contextmenu.{ContextMenu, ContextMenuEntry}
import ocelot.desktop.ui.widget.window.BasicWindow
import ocelot.desktop.ui.widget._
import ocelot.desktop.util.{DrawUtils, Orientation, WebcamCapture}

import scala.jdk.CollectionConverters.CollectionHasAsScala

class CameraWindow(cameraNode: CameraNode) extends BasicWindow {
  def camera: Camera = cameraNode.camera

  children :+= new PaddingBox(new Widget {
    override protected val layout: Layout = new LinearLayout(this, orientation = Orientation.Vertical)

    private val label = new Label {
      override def text: String = s"Camera — ${cameraNode.labelOrAddress}"
      override val isSmall: Boolean = true
    }

    children :+= new PaddingBox(label, Padding2D.equal(4))
    children :+= new PaddingBox(new Button {
      override def minimumSize: Size2D = Size2D(256, 24)
      override def maximumSize: Size2D = Size2D(512, 24)
      override def size: Size2D = Size2D(label.size.width - 8, 24)
      override def text: String = camera.webcamCapture.map(_.name).getOrElse("<No webcam available>")

      override def onClick(): Unit = {
        val menu = new ContextMenu
        for (webcam <- Webcam.getWebcams().asScala)
          menu.addEntry(new ContextMenuEntry(webcam.getName, () => cameraNode.camera.webcamCapture = WebcamCapture.getInstance(webcam)))

        root.get.contextMenus.open(menu, Vector2D(position.x, position.y + size.height))
      }
    }, Padding2D.equal(8))

    children :+= new PaddingBox(new Checkbox("Flip image horizontally",
      initialValue = camera.flipHorizontally) {
      override def onValueChanged(newValue: Boolean): Unit = camera.flipHorizontally = newValue
    }, Padding2D.equal(8))

    children :+= new PaddingBox(new Checkbox("Flip image vertically",
      initialValue = camera.flipVertically) {
      override def onValueChanged(newValue: Boolean): Unit = camera.flipVertically = newValue
    }, Padding2D.equal(8))

    children :+= new PaddingBox(new Checkbox("Unlimit call budget",
      initialValue = camera.directCalls) {
      override def onValueChanged(newValue: Boolean): Unit = camera.directCalls = newValue
    }, Padding2D.equal(8))
  }, Padding2D.equal(8))

  override def draw(g: Graphics): Unit = {
    beginDraw(g)
    DrawUtils.windowWithShadow(g, position.x, position.y, size.width, size.height, 1f, 0.5f)
    drawChildren(g)
    endDraw(g)
  }
}
