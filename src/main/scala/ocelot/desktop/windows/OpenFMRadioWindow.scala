package ocelot.desktop.windows

import ocelot.desktop.audio.{SoundSource, SoundSources}
import ocelot.desktop.color.{Color, IntColor}
import ocelot.desktop.entity.OpenFMRadio
import ocelot.desktop.geometry.{Padding2D, Size2D}
import ocelot.desktop.graphics.Graphics
import ocelot.desktop.node.nodes.{ComputerNode, OpenFMRadioNode}
import ocelot.desktop.ui.event.MouseEvent
import ocelot.desktop.ui.layout.{Alignment, Layout, LinearLayout}
import ocelot.desktop.ui.widget._
import ocelot.desktop.ui.widget.tooltip.Tooltip
import ocelot.desktop.ui.widget.window.BasicWindow
import ocelot.desktop.util.animation.UnitAnimation
import ocelot.desktop.util.{DrawUtils, Orientation}
import ocelot.desktop.{ColorScheme, OcelotDesktop}
import totoro.ocelot.brain.entity.Case
import totoro.ocelot.brain.nbt.NBTTagCompound

class OpenFMRadioWindow(radioNode: OpenFMRadioNode) extends BasicWindow {
  private val scale = 2f

  private def radio: OpenFMRadio = radioNode.openFMRadio

  override def minimumSize: Size2D = new Size2D(232 * scale, 105 * scale)
  override protected val layout: Layout = new LinearLayout(this, Orientation.Vertical)

  // Volume controls
  children :+= new PaddingBox(
    new Widget {
      override protected val layout: Layout = new LinearLayout(
        this,
        Orientation.Horizontal,
        0,
        Alignment.Start
      )

      // Volume up/down buttons
      private def addVolumeUpOrDownButton(isUp: Boolean, x: Float): Unit = {
        children :+= new PaddingBox(
          new IconButton(
            s"buttons/OpenFMRadioVolume${if (isUp) "Up" else "Down"}Off",
            s"buttons/OpenFMRadioVolume${if (isUp) "Up" else "Down"}On",
            sizeMultiplier = scale
          ) {
            override def onPressed(): Unit = {
              if (isUp)
                radio.volUp()
              else
                radio.volDown()
            }
          },
          Padding2D(0, 0, 0, x)
        )
      }

      addVolumeUpOrDownButton(false, 0)

      // Volume label
      children :+= new PaddingBox(
        new Label {
          override def maximumSize: Size2D = Size2D(20, 20)
          override def text: String = (radio.volume * 10).floor.toInt.toString
          override def color: Color = Color.White
        },
        Padding2D(2, 0, 0, 20)
      )

      addVolumeUpOrDownButton(true, 8)

      // Redstone button
      children :+= new PaddingBox(
        new IconButton(
          "buttons/OpenFMRadioRedstoneOff",
          "buttons/OpenFMRadioRedstoneOn",
          isSwitch = true,
          sizeMultiplier = scale
        ) {
          override def isOn: Boolean = radio.isListenRedstone
          override def onPressed(): Unit = radio.isListenRedstone = true
          override def onReleased(): Unit = radio.isListenRedstone = false
        },
        Padding2D(0, 0, 0, 128)
      )

      // Close button
      children :+= new PaddingBox(
        new IconButton(
          "buttons/OpenFMRadioCloseOff",
          "buttons/OpenFMRadioCloseOn",
          sizeMultiplier = scale
        ) {
          override def onPressed(): Unit = hide()
        },
        Padding2D(0, 0, 0, 10)
      )
    },
    Padding2D(8, 0, 0, 188)
  )

  // Url text input
  children :+= new PaddingBox(
    new TextInput(radio.url.getOrElse("")) {
      override def minimumSize: Size2D = Size2D(402, 20)
      override def maximumSize: Size2D = Size2D(402, 20)
      override def onConfirm(): Unit = radio.url = Option(text)
    },
    Padding2D(5, 0, 0, 32)
  )

  // Start/stop button
  children :+= new PaddingBox(
    new IconButton(
      "buttons/OpenFMRadioStartOff",
      "buttons/OpenFMRadioStopOn",
      isSwitch = true,
      sizeMultiplier = scale
    ) {
      override def isOn: Boolean = radio.isPlaying
      override def onPressed(): Unit = radio.play()
      override def onReleased(): Unit = radio.stop()
    },
    Padding2D(13, 0, 0, 208)
  )

  // Color / text titles
  children :+= new PaddingBox(
    new Widget {
      override protected val layout: Layout = new LinearLayout(
        this,
        Orientation.Horizontal,
        0,
        Alignment.Start
      )

      // Color title
      children :+= new Label {
        override def text: String = "Screen color"
        override def color: Color = Color.White
      }

      // Text title
      children :+= new Label {
        override def text: String = "Screen text"
        override def color: Color = Color.White
      }
    },
    Padding2D(10, 0, 0, 30)
  )

  // Color / text text inputs
  children :+= new PaddingBox(
    new Widget {
      override protected val layout: Layout = new LinearLayout(
        this,
        Orientation.Horizontal,
        46
      )

      // Color
      children :+= new TextInput(radio.screenColor.color.toHexString.toUpperCase) {
        override def minimumSize: Size2D = Size2D(118, 20)
        override def maximumSize: Size2D = Size2D(118, 20)
        override def onConfirm(): Unit = radio.screenColor = IntColor(Integer.parseInt(text, 16))

        override def validator(text: String): Boolean = {
          try {
            Integer.parseInt(text, 16)
            true
          }
          catch {
            case _: Throwable => false
          }
        }
      }

      // Text
      children :+= new TextInput(radio.screenText) {
        override def maximumSize: Size2D = Size2D(240, 20)
        override def onConfirm(): Unit = radio.screenText = text
      }
    },
    Padding2D(1, 0, 0, 30)
  )

  // OpenFM logo
  children :+= new PaddingBox(
    new Label {
      override def text: String = "OpenFM"
      override def color: Color = Color.White
    },
    Padding2D(22, 0, 0, 210)
  )

  override def draw(g: Graphics): Unit = {
    beginDraw(g)

    g.sprite("window/OpenFMRadio", position.x, position.y, size.width, size.height)

    drawChildren(g)
    endDraw(g)
  }
}
